import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import StatCard from '../components/StatCard';
import GraphCard from '../components/GraphCard';
import SimpleTable from '../../../shared/components/Table';
import loadProducts from '../../../products/product-list/state/selectors';
import loadProductsBrand from '../../../brands/brand-list/state/selector';
import { getProducts } from '../../../products/product-list/state/actions';
import { getBrand } from '../../../brands/brand-list/state/actions';
import { getCategories } from '../../../categories/state/actions';
import loadCategories from '../../../categories/state/selectors';
import { getUsers } from '../state/actions';
import loadUsers from '../state/selectors';
import { addUser, addCategory, addProduct } from '../api/UserServices';
import {Bar} from 'react-chartjs-2';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';
import withBase from 'core/components/withBase';
import ProfileCard from 'shared/components/ProfileCard';

require('../../../assets/Fonts/Unicorns.ttf');

const styles = {
	background: {
		backgroundColor: '#f3f3f3',
		flexGrow: 1
	},
	section1: {
		marginTop: '20px'
	},
	toolbarSecondary: {
		alignItems: 'center'
	},
	listGrid:{
		height:'500px'
	}
};

class SellerHome extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.props.loadUsers(this.props.users);
		this.props.loadCategories(this.props.categories);
		// this.props.loadProducts(this.props.products);
		this.props.loadProductsBrand(this.props.productsBrand);

		
	}


	render() {
		const { classes } = this.props;

		return (
			<div className={classes.background}>
            <ProfileCard />
			</div>
		);
	}
}
const API_ENTITIES = [
  'GET_USERS',
  'GET_CATEGORIES',
  'GET_PRODUCTS',
  'GET_BRAND',
];

const mapStateToProps = (state) => {
  const users = loadUsers(state);
//   const products = loadProducts(state);
  const productsBrands = loadProductsBrand(state);
  const categories = loadCategories(state);
  return {
    users,
    // products,
    productsBrands,
	categories,
    loading: getAPILoadingStatus(API_ENTITIES)(state),
    error: getAPIErrorMessage(API_ENTITIES)(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadUsers: bindActionCreators(getUsers, dispatch),
    // loadProducts: bindActionCreators(fetchProducts, dispatch),
    loadProductsBrand: bindActionCreators(getBrand, dispatch),
	loadCategories: bindActionCreators(getCategories, dispatch)
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withBase(SellerHome)),
);
