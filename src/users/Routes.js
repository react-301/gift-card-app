import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import Loading from 'shared/components/Spinner';
import AuthRoute from 'core/security/containers/AuthRoute';

const LoadableBuyerDashboardPage = Loadable({
  loader: () => import('users/home/containers/BuyerDashboard'),
  loading: Loading,
});

const LoadableAdminDashboardPage = Loadable({
  loader: () => import('users/home/containers/AdminDashboard'),
  loading: Loading,
});
const LoadableAdminDashboardUserListPage = Loadable({
  loader: () => import('users/home/containers/AdminDashboardUserList'),
  loading: Loading,
});
const LoadableAdminDashboardProductListPage = Loadable({
  loader: () => import('users/home/containers/AdminDashboardProductList'),
  loading: Loading,
});
const LoadableSellerHomePage = Loadable({
  loader: () => import('users/home/containers/SellerHome'),
  loading: Loading,
});

export default function() {
  return (
    <Switch>
      <Route path="/users/buyer/home" component={LoadableBuyerDashboardPage} />

      <AuthRoute
        path="/users/admin/home"
        component={LoadableAdminDashboardPage}
      />

      <AuthRoute
        path="/users/admin/users"
        component={LoadableAdminDashboardUserListPage}
      />
      <AuthRoute
        path="/users/admin/product"
        component={LoadableAdminDashboardProductListPage}
      />
      <AuthRoute path="/users/seller/home" component={LoadableSellerHomePage} />
    </Switch>
  );
}
