import watchGetUsers from '../home/state/sagas';
import watchGetCategories from '../../categories/state/sagas';
export const usersSagas = [ watchGetUsers,watchGetCategories];
