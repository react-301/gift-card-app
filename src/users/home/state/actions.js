import { GET_USERS_REQUEST, GET_USERS_SUCCESS, GET_USERS_FAILURE } from './actionTypes';

export function getUsers() {
	return { type: GET_USERS_REQUEST };
}

export function getUsersSuccess(users) {
	console.log('reducer',typeof(users),users[0])
	return { type: GET_USERS_SUCCESS, payload: users };
}

export function getUsersFailure(error) {
	return { type: GET_USERS_FAILURE, payload: { error } };
}
