import { createSelector } from 'reselect';

const loadUsers = (state) => state.users;

export default loadUsers;
