import { GET_USERS_SUCCESS } from './actionTypes';

export default function(state = [ {} ], action) {
	switch (action.type) {
		case GET_USERS_SUCCESS:
			return  action.payload;
		default:
			return state;
	}
}
