import { takeLatest, put, call, race, cancelled } from 'redux-saga/effects';
import { GET_USERS_REQUEST } from './actionTypes';
import { fetchUsers } from '../api/UserServices';
import { getUsersSuccess, getUsersFailure } from './actions';

export default function* watchGetUsers() {
	yield takeLatest(GET_USERS_REQUEST, getUsers);
	console.log('h1')
}

function* getUsers(action) {
	console.log('data1',"hii")
	try {
		console.log('before fetching Users');
		const { data } = yield call(fetchUsers);
		console.log('after fetching Users', data);
		yield put(getUsersSuccess(data));
	} catch (error) {
		yield put(getUsersFailure(error));
	}
}
