import React,{useState,useEffect} from 'react';
import './index.css';
import Icon from "@material-ui/core/Icon";
import { withStyles } from "@material-ui/core/styles";
import CountUp from 'react-countup';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import grey from '@material-ui/core/colors/red';
import AddIcon from '@material-ui/icons/Add';
import Info from '@material-ui/icons/Add';
import FormDialog from '../Dialog';
import { Link } from 'react-router-dom';

const primary = grey[50]; 
const styles = theme => ({
    button: {
      margin: theme.spacing.unit,
      top:'18%',
      maxWidth:'100%',
      width:'10%'
    },
    input: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'inline',
        width:'10%'
      },
    },
  });
const StatCard = (props) => {
    
    const { classes } = props;
    const [open, setOpen] = React.useState(false);
    const handleFilter = (filterParams) => {
        // const { getProducts } = props;
        console.log(filterParams)
		props.getData(filterParams);
	};


    return (
        
        <div className="cardStat">
        {console.log('props.data',props.type)}
            <div className="additional">
                <div className="user-card" label={props.label}>
                    <div className="level center">
                       <Icon className="icon">perm_identity</Icon>
              </div>
                </div>
                <div className="more-info" label={props.label}>
              
            <IconButton size="large" color={primary} aria-label="Add" className={classes.margin} onClick={()=>setOpen(true)}>
            <Icon className="icon">add</Icon>
        </IconButton>
        <Link to={props.type=='Users'?`/users/admin/users`:`/`}>
        {props.user=='seller'?<div/>:props.type!='Categories'?<IconButton size="large" color={primary} aria-label="Add" className={classes.margin} onClick={()=>setOpen(true)}>
            <Icon className="icon">info</Icon>
        </IconButton>:<div/>}
          </Link>
        
        <FormDialog handleClose={()=>{setOpen(false)}} open={open} type={props.type} onFilter={(e)=>handleFilter(e)}/>
       
      
                </div>
            </div>
            <div className="general">
                <p className="num"><CountUp end={props.total} /></p>
                <p className="type">{props.type}</p>
            </div>
        </div>



    );
}

export default withStyles(styles)(StatCard);