import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

function FormDialog(props) {
    const { classes, onFilter } = props;

    const [firstField, setfirstField] = useState('');
    const [secondField, setsecondField] = useState('');
    const [thirdField, setthirdField] = useState('');
    const [fourthField, setfourthField] = useState('');

    const [fifthField, setfifthField] = useState('');
    const [sixthField, setsixthField] = useState('');
    const [seventhField, setseventhField] = useState('');
    const [eightField, seteightField] = useState('');
    const [ninthField, setninthField] = useState('');


    const handleProduct = (data) => {
        console.log('hiii', data)
        onFilter(data);
    };

    let x;
    switch (props.type) {
        case 'Product': x = (<div>
            <DialogContent>
                <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Name"
                type="text"
                fullWidth
                required
                onChange={(e) => { setfirstField(e.target.value) }}
            />
             <TextField
                autoFocus
                margin="dense"
                id="brand"
                label="Brand"
                type="text"
                fullWidth
                required
                onChange={(e) => { setsecondField(e.target.value) }}
            />
                <TextField
                    autoFocus
                    margin="dense"
                    id="description"
                    label="Description"
                    type="text"
                    fullWidth
                    required
                    multiline
                    onChange={(e) => { setthirdField(e.target.value) }}
                />
                 <TextField
                    autoFocus
                    margin="dense"
                    id="url"
                    label="Image URL"
                    type="text"
                    fullWidth
                    required
                    onChange={(e) => { setfourthField(e.target.value) }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    id="Points"
                    label="Points"
                    type="number"
                    fullWidth
                    required
                    min="0"
                    onChange={(e) => { setfifthField(e.target.value) }}
                />
                 <TextField
                    autoFocus
                    margin="dense"
                    id="text"
                    label="Expiry Days"
                    type="text"
                    fullWidth
                    required
                    onChange={(e) => { setsixthField(e.target.value) }}
                />
                <TextField
                autoFocus
                margin="dense"
                id="category"
                label="category ID"
                type="number"
                fullWidth
                required
                min="0"
                onChange={(e) => { setseventhField(parseInt(e.target.value));console.log(seventhField,typeof(seventhField)) }}
            />
             <TextField
                autoFocus
                margin="dense"
                id="productId"
                label="Product id"
                type="text"
                fullWidth
                required
                onChange={(e) => { seteightField(e.target.value) }}
            />
              <TextField
                autoFocus
                margin="dense"
                id="rating"
                label="Rating"
                type="number"
                fullWidth
                required
                onChange={(e) => { seteightField(e.target.value) }}
            />

            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.handleClose()} color="primary">
                    Cancel
          </Button>
                <Button onClick={() =>{handleProduct({name:firstField,brand:secondField,desc:thirdField,imageUrl:fourthField,points:fifthField,expiryDays:sixthField,categoryId:seventhField,id:eightField,rating:ninthField}); props.handleClose()}} color="primary">
                    Add
          </Button>
            </DialogActions>
        </div>); break;
        case 'User': x = (<div>
            <DialogContent><TextField
                autoFocus
                margin="dense"
                id="name"
                label="Name"
                type="text"
                fullWidth
                required
                onChange={(e) => { setfirstField(e.target.value) }}
            />
                <TextField
                    autoFocus
                    margin="dense"
                    id="description"
                    label="Email"
                    type="email"
                    fullWidth
                    required
                    multiline
                    onChange={(e) => { setsecondField(e.target.value) }}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    id="description"
                    label="Password"
                    type="text"
                    fullWidth
                    required
                    multiline
                    onChange={(e) => { setthirdField(e.target.value) }}
                />
                <FormControl required >
        <InputLabel htmlFor="role">Role</InputLabel>
        <Select
          onChange={(e)=>setfourthField(e.target.value)}
          name="role"
          inputProps={{
            id: 'role',
          }}
         
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={'user'}>User</MenuItem>
          <MenuItem value={'admin'}>Admin</MenuItem>
          <MenuItem value={'seller'}>Seller</MenuItem>
        </Select>
        <FormHelperText>Required</FormHelperText>
      </FormControl>


            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.handleClose()} color="primary">
                    Cancel
          </Button>
                <Button onClick={() =>{handleProduct({name:firstField,email:secondField,password:thirdField,role:fourthField}); props.handleClose()}} color="primary">
                    Add
          </Button>
            </DialogActions>
        </div>); break;

        case 'Category': x = (<div>
            <DialogContent><TextField
                autoFocus
                margin="dense"
                id="name"
                label="Category"
                type="text"
                fullWidth
                required
                onChange={(e) => { setfirstField(e.target.value) }}
            />
            <TextField
                autoFocus
                margin="dense"
                id="url"
                label="Image URL"
                type="text"
                fullWidth
                required
                onChange={(e) => { setsecondField(e.target.value) }}
            />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.handleClose()} color="primary">
                    Cancel
          </Button>
                <Button onClick={() =>{handleProduct({name:firstField,imageURL:secondField}); props.handleClose()}} color="primary">
                    Add
          </Button>
            </DialogActions>
        </div>);
            break;
        default: x = (<div></div>)
    }

    return (
        <div>
            <Dialog open={props.open} onClose={() => props.handleClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add {props.type}</DialogTitle>
                {x}
            </Dialog>
        </div>
    );
}

export default FormDialog;