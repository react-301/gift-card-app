import React from 'react';
import MaterialTable from 'material-table';

export default function MaterialTableDemo(props) {
  const data1=[];
    console.log('data',props.data,typeof(props.data))
  const [state, setState] = React.useState({
    columns: [
      { title: 'ID', field: 'id' },
      { title: 'Name', field: 'name' },
      { title: 'Email', field: 'email' },
      { title: 'Role', field: 'role' },
    ],
    data: props.data,
  });

  const styles={
    container:{
      width:'80%'
    }
  }

  return (
    <MaterialTable
      title="User Data"
      columns={state.columns}
      data={props.data}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              // const data = [...state.data];
              // data.push(newData);
              // setState({ ...state, data });
              props.add(newData)
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              props.update(newData)
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            resolve();
            setTimeout(() => {
             props.delete(oldData)
            }, 600);
          }),
      }}
    />
  );
}
