import React from "react";
import './index.css';
import { withStyles } from "@material-ui/core/styles";

const styles =(props)=>({
  imgBx:{
    background:`('${props.brandImage}')`,
    backgroundSize: 'cover',
  },
});


const BrandCard = (props) => {

  return (
  <div>
        <div className="box1">
      <div className="card1">
        <div className="imgBx" style={{backgroundImage:`url(${props.brandImage})`}}>
           
        </div>
        <div className="details" >
            <h2>{props.brandName}</h2>
        </div>
      </div>
  </div>
  </div>
  );
}

export default withStyles(styles)(BrandCard);