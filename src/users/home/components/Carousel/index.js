import React from 'react';
import './index.css';
import { withRouter, Redirect } from 'react-router-dom';
require('../../../../assets/Fonts/DarkLarch_PERSONAL_USE.ttf');

export default class Carousel extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			redirect: false
		};
	}
	componentDidMount() {
		var autoUpdate = true,
			timeTrans = 3000;

		var cdSlider = document.querySelector('.cd-slider'),
			item = cdSlider.querySelectorAll('li'),
			nav = cdSlider.querySelector('nav');

		item[0].className = 'current_slide';

		for (var i = 0, len = item.length; i < len; i++) {
			var color = item[i].getAttribute('data-color');
			item[i].style.backgroundColor = color;
		}
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf('MSIE');
		if (msie > 0) {
			var version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)));
			if (version === 9) {
				cdSlider.className = 'cd-slider ie9';
			}
		}

		if (item.length <= 1) {
			nav.style.display = 'none';
		}

		function prevSlide() {
			var currentSlide = cdSlider.querySelector('li.current_slide'),
				prevElement = currentSlide.previousElementSibling,
				prevSlide = prevElement !== null ? prevElement : item[item.length - 1],
				prevColor = prevSlide.getAttribute('data-color'),
				el = document.createElement('span');

			currentSlide.className = '';
			prevSlide.className = 'current_slide';

			nav.children[0].appendChild(el);

			var size =
					cdSlider.clientWidth >= cdSlider.clientHeight
						? cdSlider.clientWidth * 2
						: cdSlider.clientHeight * 2,
				ripple = nav.children[0].querySelector('span');

			ripple.style.height = size + 'px';
			ripple.style.width = size + 'px';
			ripple.style.backgroundColor = prevColor;

			ripple.addEventListener('webkitTransitionEnd', function() {
				if (this.parentNode) {
					this.parentNode.removeChild(this);
				}
			});

			ripple.addEventListener('transitionend', function() {
				if (this.parentNode) {
					this.parentNode.removeChild(this);
				}
			});
		}

		function nextSlide() {
			var currentSlide = cdSlider.querySelector('li.current_slide'),
				nextElement = currentSlide.nextElementSibling,
				nextSlide = nextElement !== null ? nextElement : item[0],
				nextColor = nextSlide.getAttribute('data-color'),
				el = document.createElement('span');

			currentSlide.className = '';
			nextSlide.className = 'current_slide';

			nav.children[1].appendChild(el);

			var size =
					cdSlider.clientWidth >= cdSlider.clientHeight
						? cdSlider.clientWidth * 2
						: cdSlider.clientHeight * 2,
				ripple = nav.children[1].querySelector('span');

			ripple.style.height = size + 'px';
			ripple.style.width = size + 'px';
			ripple.style.backgroundColor = nextColor;

			ripple.addEventListener('webkitTransitionEnd', function() {
				if (this.parentNode) {
					this.parentNode.removeChild(this);
				}
			});

			ripple.addEventListener('transitionend', function() {
				if (this.parentNode) {
					this.parentNode.removeChild(this);
				}
			});
		}

		updateNavColor();

		function updateNavColor() {
			var currentSlide = cdSlider.querySelector('li.current_slide');

			var nextColor =
				currentSlide.nextElementSibling !== null
					? currentSlide.nextElementSibling.getAttribute('data-color')
					: item[0].getAttribute('data-color');
			var prevColor =
				currentSlide.previousElementSibling !== null
					? currentSlide.previousElementSibling.getAttribute('data-color')
					: item[item.length - 1].getAttribute('data-color');

			if (item.length > 2) {
				nav.querySelector('.prev').style.backgroundColor = prevColor;
				nav.querySelector('.next').style.backgroundColor = nextColor;
			}
		}

		nav.querySelector('.next').addEventListener('click', function(event) {
			event.preventDefault();
			nextSlide();
			updateNavColor();
		});

		nav.querySelector('.prev').addEventListener('click', function(event) {
			event.preventDefault();
			prevSlide();
			updateNavColor();
		});

		setInterval(function() {
			if (autoUpdate) {
				nextSlide();
				updateNavColor();
			}
		}, timeTrans);
	}

	handleListPage = () => {
		// alert('hi');
		// this.props.history.push(`/products`);
		this.setState({ redirect: true });
	};

	render() {
		const styles = {
			font: {
				fontFamily: 'Dark Larch PERSONAL USE ONLY'
			}
		};
		if (this.state.redirect) {
			return <Redirect to="/products" />;
		}
		return (
			<div className="cd-slider" style={{ cursor: 'pointer' }} onClick={this.handleListPage}>
				<ul>
					<li data-color="#FF384B">
						<div
							className="content"
							style={{
								backgroundImage: `url("https://fox5theatre.com/wp-content/uploads/2018/08/giftcard.png")`
							}}
						>
							<blockquote>
								<p style={{ fontFamily: 'Dark Larch PERSONAL USE ONLY', color: '#fff' }}>
									We have a gift card for every occasion
								</p>
							</blockquote>
						</div>
					</li>
					<li data-color="#FF9C00">
						<div
							className="content"
							style={{ backgroundImage: `url("http://www.kfc.bm/images/KFC-Gift-card.png")` }}
						>
							<blockquote>
								<p style={{ fontFamily: 'Dark Larch PERSONAL USE ONLY', color: '#fff' }}>
									Wanna Eat?? We have a card for you
								</p>
							</blockquote>
						</div>
					</li>
					<li data-color="#002AFF">
						<div
							className="content"
							style={{
								backgroundImage: `url("https://m.media-amazon.com/images/I/413TK1uwlIL._AC_UL480_FMwebp_QL65_.jpg")`
							}}
						>
							<blockquote>
								<p style={{ fontFamily: 'Dark Larch PERSONAL USE ONLY', color: '#fff' }}>
									Love Shopping Online
								</p>
							</blockquote>
						</div>
					</li>
				</ul>
				<nav className="nav1">
					<div>
						<a className="prev" href="#" />
					</div>
					<div>
						<a className="next" href="#" />
					</div>
				</nav>
			</div>
		);
	}
}
