import React from "react";
import './index.css';
import { withStyles } from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import Icon from "@material-ui/core/Icon";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';



const GraphCard = (props) => {

  return (
    <div className="container">
    <div className="cardGraph">
      
      {/* <i className="fas fa-arrow-right"></i> */}
      <Icon className='i'>arrow_forward</Icon>
     
      <div className="pic">
      {props.children}
      </div>
  
      <button >
      </button>
      <h2 className="h2">info</h2>
    </div>
  </div>
  );
}

export default GraphCard;