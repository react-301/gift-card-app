import httpClient from '../../../core/services/baseService/httpClient';

export function fetchUsers() {
	return httpClient.get('users');
}
export function addUser(userDetails) {
	return httpClient.post('users', userDetails);
}
export function addCategory(categoryDetails) {
	return httpClient.post('categories', categoryDetails);
}
export function addProduct(productDetails) {
	return httpClient.post('products', productDetails);
}

export function deleteUser(userDetails) {
	return httpClient.delete(`users/${userDetails}`);
}

export function updateUser(userDetails) {
	return httpClient.put(`users/${userDetails.id}`,userDetails);
}

export function editProduct(id,productDetails) {
	return httpClient.post(`products/${id}`, productDetails);
}

export function deleteProduct(id) {
	return httpClient.delete(`products/${id}`);
}

