import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './AdminDashboard.css';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import StatCard from '../components/StatCard';
import GraphCard from '../components/GraphCard';
import SimpleTable from '../../../shared/components/Table';
import loadProducts from '../../../products/product-list/state/selectors';
import loadProductsBrand from '../../../brands/brand-list/state/selector';
import { getProducts } from '../../../products/product-list/state/actions';
import { getBrand } from '../../../brands/brand-list/state/actions';
import { getCategories } from '../../../categories/state/actions';
import loadCategories from '../../../categories/state/selectors';
import { getUsers } from '../state/actions';
import loadUsers from '../state/selectors';
import { addUser, addCategory, addProduct } from '../api/UserServices';
import {Bar} from 'react-chartjs-2';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';
import withBase from 'core/components/withBase';

require('../../../assets/Fonts/Unicorns.ttf');

const styles = {
	background: {
		backgroundColor: '#f3f3f3',
		flexGrow: 1
	},
	section1: {
		marginTop: '20px'
	},
	toolbarSecondary: {
		alignItems: 'center'
	},
	listGrid:{
		height:'500px'
	}
};

class AdminDashboard extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.props.loadUsers(this.props.users);
		this.props.loadCategories(this.props.categories);
		// this.props.loadProducts(this.props.products);
		this.props.loadProductsBrand(this.props.productsBrand);

		
	}


	render() {
		const { classes } = this.props;

		const data = {
			labels: this.props.categories.map(k=>{
				return(k.name)}
			),
			datasets: [
			  {
				label: 'My First dataset',
				backgroundColor: 'rgba(255,99,132,0.2)',
				borderColor: 'rgba(255,99,132,1)',
				borderWidth: 1,
				hoverBackgroundColor: 'rgba(255,99,132,0.4)',
				hoverBorderColor: 'rgba(255,99,132,1)',
				data:[7,7,8,5,7,8,9]
			  }
			]
		  };

		return (
			<div className={classes.background}>
				{/* <Navbar classes={classes}/> */}
				<Grid container spacing={8} className={classes.marginTop}>
					<Grid item md={3} xs={6} sm={6}>
						{/* <StatCard getData={(e) => console.log('callback', e)} total={Object.keys(this.props.products).length} type={''} icon={'perm_identity'} label={'1'} /> */}
					</Grid>
					<Grid item md={3} xs={6} sm={6}>
						<StatCard user={'admin'} getData={(e) => {addUser(e);}} total={this.props.users.length} type={'Users'} icon={'perm_identity'} label={'2'} />
					</Grid>
					<Grid item md={3} xs={6} sm={6}>
						<StatCard user={'admin'} getData={(e) => {addCategory(e);}} total={this.props.categories.length} type={'Categories'} icon={'perm_identity'} label={'3'} />
					</Grid>
					<Grid item md={3} xs={6} sm={6}>
						<StatCard user={'admin'} getData={(e) =>{console.log(e);addProduct(e)}} total={136} type={'Products'} icon={'perm_identity'} label={'4'} />
					</Grid>
				</Grid>
				<Grid container spacing={8}>
					<Grid item md={6} xs={12}>
						<GraphCard>
						<Bar
          data={data}
          width={50}
          height={100}
          options={{
			maintainAspectRatio: false,
			responsive:true,
			legend:false
          }}
        />
						</GraphCard>
					</Grid>
					<Grid item md={6} xs={12}>
						<GraphCard />
					</Grid>
				</Grid>
				<Grid container spacing={8} className={classes.listGrid} >
					<Grid item md={6} xs={12}>
						<div class="profile-card js-profile-card">
							<div class="profile-card__img" label="1">
								<h1> Top Sellers <i  style={{float:'right', paddingTop:'12px'}}class="material-icons">info</i></h1>
							</div>
							<div class="tables"><SimpleTable /></div>
						</div>
					</Grid>
					<Grid item md={6} xs={12}>
						<div class="profile-card js-profile-card">
							<div class="profile-card__img" label="2">
								<h1>Top Selling Products  <i  style={{float:'right', paddingTop:'12px'}}class="material-icons">info</i></h1>
							</div>
							<div class="tables"><SimpleTable /></div>
						</div>
					</Grid>
				</Grid>
			</div>
		);
	}
}
const API_ENTITIES = [
  'GET_USERS',
  'GET_CATEGORIES',
  'GET_PRODUCTS',
  'GET_BRAND',
];

const mapStateToProps = (state) => {
  const users = loadUsers(state);
//   const products = loadProducts(state);
  const productsBrands = loadProductsBrand(state);
  const categories = loadCategories(state);
  return {
    users,
    // products,
    productsBrands,
	categories,
    loading: getAPILoadingStatus(API_ENTITIES)(state),
    error: getAPIErrorMessage(API_ENTITIES)(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadUsers: bindActionCreators(getUsers, dispatch),
    // loadProducts: bindActionCreators(fetchProducts, dispatch),
    loadProductsBrand: bindActionCreators(getBrand, dispatch),
	loadCategories: bindActionCreators(getCategories, dispatch)
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withBase(AdminDashboard)),
);
