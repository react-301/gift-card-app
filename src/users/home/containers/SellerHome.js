import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import StatCard from '../components/StatCard';
import GraphCard from '../components/GraphCard';
import SimpleTable from '../../../shared/components/Table';
import loadProducts from '../../../products/product-list/state/selectors';
import loadProductsBrand from '../../../brands/brand-list/state/selector';
import { getProducts } from '../../../products/product-list/state/actions';
import { getBrand } from '../../../brands/brand-list/state/actions';
import { getCategories } from '../../../categories/state/actions';
import loadCategories from '../../../categories/state/selectors';
import { getUsers } from '../state/actions';
import loadUsers from '../state/selectors';
import { addUser, addCategory, addProduct, editProduct, deleteProduct } from '../api/UserServices';
import { Bar } from 'react-chartjs-2';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';
import withBase from 'core/components/withBase';
import ProfileCard from 'shared/components/ProfileCard';
import GiftControlCard from 'shared/components/ProductCard/productCard.js';

require('../../../assets/Fonts/Unicorns.ttf');

const styles = {
	background: {
		backgroundColor: '#f3f3f3',
		flexGrow: 1,

	},
	section1: {
		padding: '20px'
	},
	toolbarSecondary: {
		alignItems: 'center'
	},
	listGrid: {
		height: '500px'
	},
	cardSection: {
		padding: '5px'
	}
};

class SellerHome extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.props.loadUsers(this.props.users);
		this.props.loadCategories(this.props.categories);
		// this.props.loadProducts(this.props.products);
		this.props.loadProductsBrand(this.props.productsBrand);


	}


	render() {
		const { classes } = this.props;

		return (
			<div className={classes.background}>
				<Grid container spacing={8} className={classes.section1}>
					<Grid item md={3} xs={12} sm={12}>
						<Grid item md={12} xs={12} sm={12}>
							<ProfileCard />
						</Grid>
						<Grid item md={12} xs={12} sm={12}>
							<StatCard user={'seller'} getData={(e) => console.log('callback', e)} total={12} type={'Product'} icon={'perm_identity'} label={'4'} />

						</Grid>
					</Grid>
					<Grid md={9} xs={12} sm={12} container className={classes.section1}>

						{/* {this.props.allProducts.length > 0 ? this.props.allProducts.map(k => {
							return <Grid className={classes.cardSection} item md={4} xs={12} sm={12}><GiftControlCard role={'seller'} editData={(e) => editProduct(e)} deleteData={(e) => deleteProduct(e)} value={k} /></Grid>
						})
							: <div>No products found</div>} */}


					</Grid>
				</Grid>
			</div>
		);
	}
}
const API_ENTITIES = [
	'GET_USERS',
	'GET_CATEGORIES',
	'GET_PRODUCTS',
	'GET_BRAND',
];

const mapStateToProps = (state) => {
	const users = loadUsers(state);
	  const products = loadProducts(state);
	const productsBrands = loadProductsBrand(state);
	const categories = loadCategories(state);
	return {
		users,
		products,
		productsBrands,
		categories,
		loading: getAPILoadingStatus(API_ENTITIES)(state),
		error: getAPIErrorMessage(API_ENTITIES)(state),
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadUsers: bindActionCreators(getUsers, dispatch),
		// loadProducts: bindActionCreators(fetchProducts, dispatch),
		loadProductsBrand: bindActionCreators(getBrand, dispatch),
		loadCategories: bindActionCreators(getCategories, dispatch)
	};
};

export default withStyles(styles)(
	connect(
		mapStateToProps,
		mapDispatchToProps,
	)(withBase(SellerHome)),
);
