import React,{useState} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './AdminDashboard.css';
import { withStyles } from '@material-ui/core/styles';
// import Grid from '@material-ui/core/Grid';
// import StatCard from '../components/StatCard';
// import GraphCard from '../components/GraphCard';
import SimpleTable from '../../../shared/components/Table';
import loadProducts from '../../../products/product-list/state/selectors';
import loadProductsBrand from '../../../brands/brand-list/state/selector';
import { getProducts } from '../../../products/product-list/state/actions';
import { getBrand } from '../../../brands/brand-list/state/actions';
import { getCategories } from '../../../categories/state/actions';
import loadCategories from '../../../categories/state/selectors';
import { getUsers } from '../state/actions';
import loadUsers from '../state/selectors';
import Card from '@material-ui/core/Card';
import { addUser, addCategory, addProduct,deleteUser,updateUser } from '../api/UserServices';
import UserTable from '../components/UserTable/index';
require('../../../assets/Fonts/Unicorns.ttf');

const styles = {
	background: {
		backgroundColor: '#f3f3f3',
		flexGrow: 1,
		alignItems:'center',
		padding:'20px',
		paddingLeft:'auto',
		paddingRight:'auto'
	},
	container:{
		

	}
};

class AdminDashboardUserList extends React.Component {
	constructor(props) {
		super(props);
		this.state={
			userData:[]
		}
	}
	componentDidMount() {
		this.props.loadUsers(this.props.users);
		// this.props.loadCategories(this.props.categories);
		// this.props.loadProducts(this.props.products);
		// this.props.loadProductsBrand(this.props.productsBrand);

		
	}

	render() {
		console.log('user',this.props.users)
		let data1=[];
		Object.keys(this.props.users).map(k=>
			{  console.log('data',this.props.users[k])
				data1.push(this.props.users[k])}
				);
		const { classes } = this.props;

		return (
			<div className={classes.background}>
			<div className={classes.container}>
			{data1.length>0?<UserTable data={data1} add={(e)=>addUser(e)} delete={(e)=>{deleteUser(e.id)}} update={(e)=>updateUser(e)}/>:<div/>}
</div>
		 
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	const users = loadUsers(state);
	// const products = loadProducts(state);
	// const productsBrands = loadProductsBrand(state);
	// const categories = loadCategories(state);
	return {
		users,
		// products,
		// productsBrands,
		// categories
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadUsers: bindActionCreators(getUsers, dispatch),
		// loadProducts: bindActionCreators(getProducts, dispatch),
		// loadProductsBrand: bindActionCreators(getBrand, dispatch),
		// loadCategories: bindActionCreators(getCategories, dispatch)
	};
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(AdminDashboardUserList));
