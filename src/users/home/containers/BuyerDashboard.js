import React from 'react';
import ProfileCard from '../../../shared/components/ProfileCard';
import BrandCard from '../components/BrandCard/index';
import Navbar from '../../../core/app-shell/components/NavBar/index';
import Carousel from '../components/Carousel';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import brandBackgroundImage from '../../../assets/images/1976241.jpg';
import brand1 from '../../../assets/images/brands/bms.jpg';
import brand2 from '../../../assets/images/brands/chroma.jpg';
import brand3 from '../../../assets/images/brands/play.jpg';
import brand4 from '../../../assets/images/brands/pvr.jpg';
import brand5 from '../../../assets/images/brands/shopperStop.jpg';
import brand6 from '../../../assets/images/brands/uber.jpg';
import CategoryGrid from '../components/CategoryGrid';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCategories } from '../../../categories/state/actions';
import loadCategories from '../../../categories/state/selectors';
import withBase from 'core/components/withBase';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';

require('../../../assets/Fonts/Unicorns.ttf');

const styles = {
	background: {
		backgroundColor: '#f3f3f3',
		flexGrow: 1
	},
	brandHeader: {
		textAlign: 'center',
		fontSize: '60px',
		fontFamily: 'Unicorns are Awesome',
		margin: '40px auto',
		color: '#000'
	},
	brandBackground: {
		background: `linear-gradient(rgba(255,255,255,3.5), rgba(255,255,255,0.5)),url('${brandBackgroundImage}')`,
		backgroundSize: 'cover'
	},
	gridPadding: {
		padding: '0px !important'
	}
};

class BuyerDashboard extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.props.loadCategories(this.props.categories);
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.background}>
				<Grid container spacing={24}>
					<Grid item xs={12} className={classes.gridPadding}>
						<Carousel />
					</Grid>
				</Grid>
				<div className={classes.brandBackground}>
					<h1 className={classes.brandHeader}>Some Popular Brands</h1>
					<Grid container spacing={24} className={classes.gridPadding}>
						<Grid item xs={12} md={4}>
							<BrandCard brandName={'Amazon'} brandImage={brand1} />
						</Grid>
						<Grid item xs={12} md={4}>
							<BrandCard brandName={'Amazon'} brandImage={brand2} />
						</Grid>
						<Grid item xs={12} md={4}>
							<BrandCard brandName={'Amazon'} brandImage={brand3} />
						</Grid>
						<Grid item xs={12} md={4}>
							<BrandCard brandName={'Amazon'} brandImage={brand4} />
						</Grid>
						<Grid item xs={12} md={4}>
							<BrandCard brandName={'Amazon'} brandImage={brand5} />
						</Grid>
						<Grid item xs={12} md={4}>
							<BrandCard brandName={'Amazon'} brandImage={brand6} />
						</Grid>
					</Grid>
				</div>
				<Grid container spacing={24} className={classes.gridPadding}>
					<h1 className={classes.brandHeader}>Shop By Categories</h1>
					<Grid item xs={12} md={12}>
						{console.log('data1', this.props.categories)}
						<CategoryGrid data={this.props.categories} />
					</Grid>
				</Grid>
			</div>
		);
	}
}
// const API_ENTITIES = ['GET_CATEGORIES'];

const mapStateToProps = (state) => {
	const categories = loadCategories(state);
	return {
		categories
		// loading: getAPILoadingStatus(API_ENTITIES)(state),
		// error: getAPIErrorMessage(API_ENTITIES)(state),
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadCategories: bindActionCreators(getCategories, dispatch)
	};
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(withBase(BuyerDashboard)));
