import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import ProductRoutes from './products/Routes';
import CategoryRoutes from './categories/Routes';
import UsersRoutes from 'users/Routes';
import AppShell from './core/app-shell/containers/AppShell';
import Loadable from 'react-loadable';
import Loading from 'shared/components/Spinner';
import NotFound from 'core/error/components/NotFound';
import ErrorBoundary from 'core/error/components/ErrorBoundary';
import AuthRoute from 'core/security/containers/AuthRoute';

const LoadableProductCartPage = Loadable({
  loader: () => import('products/product-cart/containers/ProductCartPage'),
  loading: Loading,
});

const LoadableRegisterPage = Loadable({
  loader: () => import('core/security/containers/RegisterPage'),
  loading: Loading,
});

const LoadableLoginPage = Loadable({
  loader: () => import('core/security/containers/LoginPage'),
  loading: Loading,
});

export default function(props) {
  const { newServiceWorkerDetected, onLoadNewServiceWorkerAccept } = props;
  return (
    <BrowserRouter>
      <ErrorBoundary>
        <AppShell
          newServiceWorkerDetected={newServiceWorkerDetected}
          onLoadNewServiceWorkerAccept={onLoadNewServiceWorkerAccept}
        >
          <Switch>
            <Redirect exact from="/" to="users/buyer/home" />
            <Route path="/login" component={LoadableLoginPage} />
            <Route path="/register" component={LoadableRegisterPage} />
            <Route path="/users" component={UsersRoutes} />
            <Route path="/products" component={ProductRoutes} />
            <Route path="/categories" component={CategoryRoutes} />
            <AuthRoute path="/cart" component={LoadableProductCartPage} />
            <Route component={NotFound} />
          </Switch>
        </AppShell>
      </ErrorBoundary>
    </BrowserRouter>
  );
}
