console.log('custom service worker kick off...');

var CACHE_NAME = 'giftapp-api-cache-v2';

// Add a listener to receive messages from clients
self.addEventListener('message', function(event) {
  // Force SW upgrade (activation of new installed SW version)
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
  }
});

self.addEventListener('fetch', function(event) {
  console.log('[ServiceWorker] Fetch', event.request.url);
  const cacheName = workbox.core.cacheNames.runtime;
  var cacheDataFor = [
    'products',
    'brands',
    'categories',
    'fonts.googleapis.com',
    'fonts.gstatic.com',
    '.jpg',
    '.jpeg',
    '.png',
  ];
  if (
    event.request.method === 'GET' &&
    //event.request.url.indexOf(cacheURLs) > 0
    cacheDataFor.some(
      (cacheElement) => event.request.url.indexOf(cacheElement) > 0,
    )
  ) {
    event.respondWith(
      caches
        .match(event.request)
        // response is complete http headers + content
        .then(function(response) {
          console.log('could find asset in cache ', event.request.url);
          // Cache hit - return response
          if (response) {
            return response;
          }
          console.log('could not find asset ', event.request.url);
          // invoked only if cache not found
          return fetch(event.request).then(function(response) {
            console.log('got reply from server, caching ondemand ');
            // Check if we received a valid response
            if (
              !response ||
              response.status !== 200 ||
              response.type !== 'basic'
            ) {
              return response;
            }

            // IMPORTANT: Clone the response. A response is a stream
            // and because we want the browser to consume the response
            // as well as the cache consuming the response, we need
            // to clone it so we have two streams.
            var responseToCache = response.clone();

            caches.open(cacheName).then(function(cache) {
              cache.put(event.request, responseToCache);
            });

            return response;
          });
        }),
    );
  }
});
