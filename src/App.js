import React from 'react';
import './App.css';

import RouteComponent from './Routes';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.registration = false;
    this.state = {
      initializing: true,
      newServiceWorkerDetected: false,
    };
  }

  componentDidMount() {
    document.addEventListener(
      'onNewServiceWorker',
      this.handleNewServiceWorker.bind(this),
    );
  }

  componentWillUnmount() {
    document.removeEventListener(
      'onNewServiceWorker',
      this.handleNewServiceWorker.bind(this),
    );
  }

  handleNewServiceWorker(event) {
    this.registration = event.detail.registration;
    this.setState({
      ...this.state,
      newServiceWorkerDetected: true,
    });
  }

  render() {
    const {
      newServiceWorkerDetected,

      initializing,
    } = this.state;
    return (
      <div
        className="App"
        style={{ backgroundColor: 'rgba(0, 79, 147, 0.15)' }}
      >
        <RouteComponent
          newServiceWorkerDetected={newServiceWorkerDetected}
          onLoadNewServiceWorkerAccept={this.handleLoadNewServiceWorkerAccept.bind(
            this,
          )}
        />
      </div>
    );
  }

  handleLoadNewServiceWorkerAccept() {
    this.props.onLoadNewServiceWorkerAccept(this.registration);
  }
}

export default App;
