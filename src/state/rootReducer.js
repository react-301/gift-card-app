import { combineReducers } from 'redux';
// import * as coreReducers from '../core/state/reducers';

import product from '../products/product-detail/state/reducers';
import productReviews from '../shared/state/ReviewList/state/reducer';
import products from '../products/product-list/state/reducers';
import cartProducts from '../products/product-cart/state/reducers';
import categories from '../categories/state/reducers';
import productsBrand from '../brands/brand-list/state/reducer';
import core from '../core/state/reducers';
import users from '../users/home/state/reducers';

export default combineReducers({
	...core,
	product,
	cartProducts,
	productReviews,
	products,
	productsBrand,
	categories,
	users
});
