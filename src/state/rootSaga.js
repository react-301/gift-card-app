import { call, all } from 'redux-saga/effects';
import { makeRestartable } from '../core/state/sagas/sagaHelpers';
import { coreSagas } from '../core/state/sagas/coreSagas';
import { productSagas } from '../products/state/sagas';
// import { watchGetCategories } from '../categories/state/sagas';
import {usersSagas} from '../users/state/sagas';
// import { watchGetProductsBrand } from '../brands/brand-list/state/sagas';

const domainSagas = [ ...coreSagas, ...productSagas, ...usersSagas ]; // keep this list in separate js file and import it

const rootSagas = domainSagas.map(makeRestartable);

export default function* root() {
	yield all(rootSagas.map((saga) => call(saga)));
}
