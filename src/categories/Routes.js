import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import Loading from 'shared/components/Spinner';

const LoadableProductListPage = Loadable({
  loader: () => import('products/product-list/containers/ProductListPage'),
  loading: Loading,
});

export default function() {
  return (
    <Switch>
      <Route
        exact
        path="/categories/:categoryId/products"
        component={LoadableProductListPage}
      />
    </Switch>
  );
}
