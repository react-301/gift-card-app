import { createSelector } from 'reselect';

const loadCategories = (state) => state.categories;

export default loadCategories;
