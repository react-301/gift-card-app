import { GET_CATEGORIES_SUCCESS } from './actionTypes';

export default function(state = [ {} ], action) {
	switch (action.type) {
		case GET_CATEGORIES_SUCCESS:
			return action.payload;
		default:
			return state;
	}
}
