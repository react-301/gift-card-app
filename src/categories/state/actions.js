import { GET_CATEGORIES_REQUEST, GET_CATEGORIES_SUCCESS, GET_CATEGORIES_FAILURE } from './actionTypes';

export function getCategories() {
	return { type: GET_CATEGORIES_REQUEST };
}

export function getCategoriesSuccess(categories) {
	return { type: GET_CATEGORIES_SUCCESS, payload: categories };
}

export function getCategoriesFailure(error) {
	return { type: GET_CATEGORIES_FAILURE, payload: { error } };
}
