import { takeLatest, put, call, race, cancelled } from 'redux-saga/effects';
import { GET_CATEGORIES_REQUEST } from './actionTypes';
import { fetchCategories } from '../api/services';
import { getCategoriesSuccess, getCategoriesFailure } from './actions';

export default function* watchGetCategories() {
	yield takeLatest(GET_CATEGORIES_REQUEST, getCategories);
	console.log('h1')
}

function* getCategories(action) {
	try {
		console.log('before fetching Categories');
		const { data } = yield call(fetchCategories);
		console.log('after fetching Categories', data);
		yield put(getCategoriesSuccess(data));
	} catch (error) {
		yield put(getCategoriesFailure(error));
	}
}
