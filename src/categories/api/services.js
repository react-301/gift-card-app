import httpClient from '../../core/services/baseService/httpClient';

export function fetchCategories() {
	return httpClient.get('categories');
}
