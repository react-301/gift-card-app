import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import store from './state/store';
import { IntlProvider } from 'react-intl';

import { addLocaleData } from 'react-intl';
import locale_en from 'react-intl/locale-data/en';
import locale_de from 'react-intl/locale-data/de';

import messages_de from './translations/de.json';
import messages_en from './translations/en.json';

addLocaleData([...locale_en, ...locale_de]);

const messages = {
  de: messages_de,
  en: messages_en,
};
const language = navigator.language.split(/[-_]/)[0];

// Callback to call when user accepts loading new service worker
// - Send message to SW to trigger the update
// - Once updated, reload this window to load new assets
const updateSW = (registration) => {
  if (registration.waiting) {
    // When the user asks to refresh the UI, we'll need to reload the window
    var preventDevToolsReloadLoop;
    navigator.serviceWorker.addEventListener('controllerchange', function(
      event,
    ) {
      // Ensure refresh is only called once.
      // This works around a bug in "force update on reload".
      if (preventDevToolsReloadLoop) {
        return;
      }

      preventDevToolsReloadLoop = true;
      console.log('Controller loaded');
      global.location.reload(true);
    });

    // Send a message to the new serviceWorker to activate itself
    registration.waiting.postMessage('skipWaiting');
  }
};

ReactDOM.render(
  <IntlProvider locale={language} messages={messages[language]}>
    <Provider store={store}>
      <App onLoadNewServiceWorkerAccept={updateSW} />
    </Provider>
  </IntlProvider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.register();
serviceWorker.register({
  // When new ServiceWorker is available, trigger an event on `document`,
  // passing `registration` as extra data
  onUpdate: (registration) => {
    var cusevent = new CustomEvent('onNewServiceWorker', {
      detail: { registration },
    });
    document.dispatchEvent(cusevent);
  },
});
