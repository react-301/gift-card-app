import React, { Component } from 'react';

import WithErrorHandling from 'core/error/components/withErrorHandling';
import LoadingOverlay from 'react-loading-overlay';

export default function withBase(WrappedComponent) {
  class baseHOC extends React.Component {
    render() {
      console.log('BaseComponent::render');
      const { loading, error } = this.props;
      return (
        <LoadingOverlay active={loading} spinner text="Hold on...">
          <WithErrorHandling showError={error} error={error}>
            <WrappedComponent {...this.props} />
          </WithErrorHandling>
        </LoadingOverlay>
      );
    }
  }

  return baseHOC;
}
// return class BaseComponent extends WrappedComponent {
//   constructor(props) {
//     super(props);
//   }
//   componentDidMount() {
//     console.log('BaseComponent::componentDidMount');
//     super.componentDidMount();
//   }
//   shouldComponentUpdate(nextProps, nextState) {
//     if (super.shouldComponentUpdate)
//       return super.shouldComponentUpdate(nextProps, nextState);
//     return true;
//   }
//   componentDidUpdate(prevProps, prevState, snapshot) {
//     console.log('BaseComponent::componentDidUpdate');
//     if (super.componentDidUpdate)
//       super.componentDidUpdate(prevProps, prevState, snapshot);
//   }
//   render() {
//     console.log('BaseComponent::render');
//     if (this.props.error) {
//       return (
//         <React.Fragment>
//           <Typography variant="body1" gutterBottom>
//             Unexpected error occured. Please contact system admin
//           </Typography>
//         </React.Fragment>
//       );
//     }
//     if (this.props.loading) {
//       return <Spinner />;
//     }
//     return super.render();
//   }
// };
