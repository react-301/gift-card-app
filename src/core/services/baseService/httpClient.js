import axios from 'axios';
import { isEmpty } from 'lodash';

import store from '../../../state/store';
import { getAuthToken } from '../../security/state/selectors';

//const API_ENDPOINT = 'http://localhost:5000/';
const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT;
//const API_ENDPOINT = 'https://react301-giftcardserver.herokuapp.com/';

const instance = axios.create({
  //baseURL: process.env.API_URL,
  baseURL: API_ENDPOINT,
  timeOut: 5000,
});

const onRequestSuccess = (config) => {
  console.log('request success', config);
  const state = store.getState();
  const token = getAuthToken(state);
  // const token = '12345';
  if (!isEmpty(token)) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
};
const onRequestFail = (error) => {
  console.log('request error', error);
  return Promise.reject(error);
};
instance.interceptors.request.use(onRequestSuccess, onRequestFail);

const onResponseSuccess = (response) => {
  console.log('response success', response, response.headers['x-total-count']);
  return response;
};
const onResponseFail = (error) => {
  console.log('response error', error);
  if (error.status || error.response) {
    const status = error.status || error.response.status;
    if (status === 403 || status === 401) {
      //dispatch action logout TODO
    }
  }

  return Promise.reject(error);
};
instance.interceptors.response.use(onResponseSuccess, onResponseFail);

export default instance;
