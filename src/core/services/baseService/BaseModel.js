export default class BaseModel {
  getType() {
    return this.constructor.name;
  }
}
