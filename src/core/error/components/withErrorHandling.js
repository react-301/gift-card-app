import React from 'react';
import Typography from '@material-ui/core/Typography';

const withErrorHandling = ({ showError, error, children }) => {
  return (
    <React.Fragment>
      {showError && (
        <Typography variant="body1" gutterBottom color="error">
          {(error.response && error.response.data) || error.message}
        </Typography>
      )}
      {children}
    </React.Fragment>
  );
};

export default withErrorHandling;
