import React from 'react';
import Typography from '@material-ui/core/Typography';

const NotFound = () => (
  <Typography variant="h3" gutterBottom>
    Ops!... Page Not Found!...
  </Typography>
);

export default NotFound;
