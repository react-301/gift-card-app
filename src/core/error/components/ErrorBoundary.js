import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      info: null,
    };
  }

  // static getDerivedStateFromError(error) {
  //   // Update state so the next render will show the fallback UI.
  //   return { hasError: true };
  // }

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, info);
    this.setState({
      error: error,
      info: info,
    });
  }

  render() {
    if (this.state.error) {
      // Some error was thrown. Let's display something helpful to the user
      return (
        <React.Fragment>
          <Typography variant="body1" gutterBottom>
            Unexpected error occured. Please contact system admin
            <h5>{this.state.error.message}</h5>
            <a href="/">Click Here</a> to get back to Home
            <details style={{ whiteSpace: 'pre-wrap' }}>
              {this.state.info.componentStack}
            </details>
          </Typography>
        </React.Fragment>
      );
    }
    // No errors were thrown. As you were.
    return this.props.children;
  }
}

export default withRouter(ErrorBoundary);
