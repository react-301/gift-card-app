import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';

const styles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing * 2,
  },
  title: {
    flexGrow: 1,
  },
});

function Header({
  classes,
  loggedIn,
  newServiceWorkerDetected,
  onLoadNewServiceWorkerAccept,
}) {
  // const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  function handleMenu(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleClickUpdate(e) {
    e.preventDefault();
    //this.handleMenuStateChange(false);
    onLoadNewServiceWorkerAccept();
    // this.props.sendEvent('Menu', 'Update!');
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <Link
              to="/"
              variant="body1"
              style={{ textDecoration: 'none', color: '#ffffff' }}
            >
              YoYo Gifts
            </Link>
          </Typography>
          {newServiceWorkerDetected ? (
            <a
              href="/"
              style={{ color: 'green' }}
              title={'New version available!'}
              onClick={handleClickUpdate.bind(this)}
            >
              {/* <FontAwesomeIcon
                icon={faUpgrade}
                style={{ marginRight: '.5em' }}
              /> */}
              <span>{'Update!'}</span>
            </a>
          ) : null}
          {loggedIn ? (
            <div>
              <IconButton className={classes.menuButton} color="inherit">
                <Link
                  to="/cart"
                  variant="body1"
                  style={{ textDecoration: 'none', color: '#ffffff' }}
                >
                  <Icon>
                    <ShoppingCart />
                  </Icon>
                </Link>
              </IconButton>

              <IconButton
                aria-owns={open ? 'menu-appbar' : undefined}
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>Wishlist</MenuItem>
                <MenuItem onClick={handleClose}>Change Password</MenuItem>
              </Menu>
            </div>
          ) : (
            <div>
              <Button color="inherit">
                <Link
                  to="/register"
                  variant="body1"
                  style={{ textDecoration: 'none', color: '#ffffff' }}
                >
                  Register
                </Link>
              </Button>
              <Button color="inherit">
                <Link
                  to="/login"
                  variant="body1"
                  style={{ textDecoration: 'none', color: '#ffffff' }}
                >
                  Login
                </Link>
              </Button>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withStyles(styles)(Header);
