import React from 'react';
import Header from './index';
import ReactRouterEnzymeContext from 'react-router-enzyme-context';
import { shallow } from 'enzyme';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { MemoryRouter } from 'react-router-dom';

describe('Header component Suite', () => {
  //   let options;
  //   beforeEach(() => {
  //     options = new ReactRouterEnzymeContext();
  //   });

  fit('renders without exploding', () => {
    const options = new ReactRouterEnzymeContext();
    const wrapper = shallow(<Header />, options.get());
    // debugger;
    expect(wrapper.find('Header')).toExist();
  });
  fit('renders with proper links before login', () => {
    const wrapper = mount(
      <MemoryRouter>
        <Header />
      </MemoryRouter>,
    );

    expect(wrapper.find('Link').length).toBe(3);
    expect(wrapper.find({ to: '/register' })).toExist();
    expect(wrapper.find({ to: '/login' })).toExist();
    expect(wrapper.find({ to: '/login' })).toExist();
  });

  it('about addMember test', () => {
    let wrapper = mount(<About />);

    let component = wrapper.instance();

    component.addMember(); // 3rd item added to state
    // this trigger/calls render.
    wrapper.update();

    expect(wrapper.find('ul').length).toBe(1);
    expect(wrapper.find('li').length).toBe(3);
  });

  it('about empty Members test', () => {
    let wrapper = mount(<About />);
    let component = wrapper.instance();
    component.empty();
    wrapper.update();

    expect(wrapper.find('ul').length).toBe(1);
    expect(wrapper.find('li').length).toBe(0);
  });

  it('about show/hide test', () => {
    let wrapper = mount(<About />);
    let component = wrapper.instance();

    //make sure it is visible
    expect(component.state.showList).toBe(true);

    //toggle first time
    component.showHide();
    wrapper.update();

    //make sure that no ul or li element present
    expect(component.state.showList).toBe(false);

    expect(wrapper.find('ul').length).toBe(0);
    expect(wrapper.find('li').length).toBe(0);
  });

  it('Likes test ', () => {
    let wrapper = mount(<About />);
    let component = wrapper.instance();

    //wrapper.find("#up").prop('onClick')();
    // or
    //wrapper.find("#up").props().onClick();

    // or hardcoded array index
    // wrapper.find("button").at(0).simulate('click');

    // or
    wrapper.find('#up').simulate('click'); // the only simulate click I want

    wrapper.update();
    expect(component.state.pageLikes).toBe(1);
    expect(wrapper.find('span').text()).toBe('1');
  });

  it('Likes test with findWhere ', () => {
    let wrapper = mount(<About />);
    let component = wrapper.instance();

    wrapper.find('.downBtn').simulate('click');

    wrapper.update();
    expect(component.state.pageLikes).toBe(-1);
    expect(wrapper.find('span').text()).toBe('-1');
    expect(wrapper.find('span').text()).toContain('-1');
  });
});
