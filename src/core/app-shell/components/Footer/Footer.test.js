import React from 'react';
import Footer from './index';
import renderer from 'react-test-renderer';

describe('Footer Test Suite', () => {
  it('footer renders correctly', () => {
    const classes = { footer: { backgroundColor: 'green' } };

    // a virtual dom in json
    const tree = renderer.create(<Footer classes={classes} />).toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree).toMatchSnapshot();
  });
});
