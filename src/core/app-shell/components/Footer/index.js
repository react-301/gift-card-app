import React from 'react';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';

const Footer = ({ classes }) => (
	<footer className={classes.footer} style={{ height: '81px' }}>
		<Typography variant="caption" align="center" gutterBottom>
			<FormattedMessage
				id="footer.copyRight"
				defaultMessage="Copyright Reserved@2019 MindTree Ltd."
				description="Copy Right text shown in the footer"
			/>
		</Typography>
	</footer>
);

export default Footer;
