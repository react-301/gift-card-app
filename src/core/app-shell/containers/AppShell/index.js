import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
// import NavBar from '../../components/NavBar';
// import loadProducts from '../../../../products/product-list/state/selectors';
// import { getProducts } from '../../../../products/product-list/state/actions';
import { useState } from 'react';
import { connect } from 'react-redux';

import { isAuthenticated } from '../../../security/state/selectors';
import { bindActionCreators } from 'redux';
const styles = (theme) => ({
  layout: {
    width: 'auto',
    // marginLeft: theme.spacing.unit * 3,
    // marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      // width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  toolbarMain: {
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
  },
  mainFeaturedPost: {
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing.unit * 4,
  },
  mainFeaturedPostContent: {
    padding: `${theme.spacing.unit * 6}px`,
    [theme.breakpoints.up('md')]: {
      paddingRight: 0,
    },
  },
  mainGrid: {
    marginTop: theme.spacing.unit * 3,
  },
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
  markdown: {
    padding: `${theme.spacing.unit * 3}px 0`,
  },
  sidebarAboutBox: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.grey[200],
  },
  sidebarSection: {
    marginTop: theme.spacing.unit * 3,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    // marginTop: theme.spacing.unit * 8,
    padding: `${theme.spacing.unit * 6}px 0`,
    bottom: 0,
    position: 'relative',
    width: '100%',
  },
});

const AppShell = (props) => {
  const {
    classes,
    children,
    newServiceWorkerDetected,
    onLoadNewServiceWorkerAccept,
  } = props;

  return (
    <React.Fragment>
      <CssBaseline />
      <div className={classes.layout}>
        <Header
          loggedIn={props.loggedIn}
          newServiceWorkerDetected={newServiceWorkerDetected}
          onLoadNewServiceWorkerAccept={onLoadNewServiceWorkerAccept}
        />
        <main>{children}</main>
      </div>
      <Footer classes={classes} />
    </React.Fragment>
  );
};

// isAuthenticated: isAuthenticated(state),
const mapStateToProps = (state) => {
  // const products = loadProducts(state);
  return {
    loggedIn: isAuthenticated(state),
    // products
  };
};

// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		loadProducts: bindActionCreators(getProducts, dispatch)
// 	};
// };

AppShell.propTypes = {
  classes: PropTypes.object.isRequired,
};

export const AppShellWithStyle = withStyles(styles)(AppShell);
export default connect(mapStateToProps)(AppShellWithStyle);
