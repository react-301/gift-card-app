import React from 'react';
import { shallow } from 'enzyme';
import { AppShellWithStyle } from './index';

describe('AppShell Test Suite', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<AppShellWithStyle />);
    // debugger;
    expect(wrapper.find('AppShell')).toExist();
  });
});
