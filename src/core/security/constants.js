import { FormattedMessage } from 'react-intl';

import React from 'react';

export const ENTER_NAME = (
  <FormattedMessage
    id="register.enterName"
    defaultMessage="Enter a name"
    description="Placeholder for name field in register"
  />
);

export const ENTER_EMAIL = (
  <FormattedMessage
    id="register.enterEmail"
    defaultMessage="Enter your email"
    description="Placeholder for email field in register"
  />
);

export const ENTER_PASSWORD = (
  <FormattedMessage
    id="register.enterPassword"
    defaultMessage="Enter your password"
    description="Placeholder for password field in register"
  />
);

export const ENTER_CONFIRM_PASSWORD = (
  <FormattedMessage
    id="register.enterConfirmPassword"
    defaultMessage="Confirm your password"
    description="Placeholder for confirm password field in register"
  />
);

export const ERROR_EMAIL_INVALID = (
  <FormattedMessage
    id="register.invalidEmail"
    defaultMessage="Enter a valid email"
    description="Error Message for invalid email"
  />
);

export const ERROR_NAME_REQUIRED = (
  <FormattedMessage
    id="error.nameRequired"
    defaultMessage="Name is required"
    description="Error Message for Name required"
  />
);

export const ERROR_EMAIL_REQUIRED = (
  <FormattedMessage
    id="error.emailRequired"
    defaultMessage="Email is required"
    description="Error Message for Email required"
  />
);

export const ERROR_PASSWORD_REQUIRED = (
  <FormattedMessage
    id="error.passwordRequired"
    defaultMessage="Password is required"
    description="Error Message for password required"
  />
);
export const ERROR_CONFIRM_PASSWORD_REQUIRED = (
  <FormattedMessage
    id="error.confirmPasswordRequired"
    defaultMessage="Confirm Password is required"
    description="Error Message for confirm password required"
  />
);

export const ERROR_PASSWORD_INVALID = (
  <FormattedMessage
    id="error.passwordPattern"
    defaultMessage="Password must contain atleast 8 characters"
    description="Error Message for password pattern"
  />
);

export const ERROR_CONFIRM_PASSWORD_MISMATCH = (
  <FormattedMessage
    id="error.confirmPwdMisMatch"
    defaultMessage="Password does not match"
    description="Error Message for confirm password mismatch"
  />
);
