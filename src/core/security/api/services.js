import httpClient from '../../services/baseService/httpClient';

// const API_ENDPOINT = 'http://localhost:5000/';

// eslint-disable-next-line import/prefer-default-export
export function authenticate(email, password) {
  return httpClient.post('login', { email, password });
}

export function register(userDetails) {
  return httpClient.post('register', { ...userDetails });
}

export function getUserByEmail(email) {
  return httpClient.get('users?email=' + email);
}
