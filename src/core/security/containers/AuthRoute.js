import { isAuthenticated } from 'core/security/state/selectors';
import { connect } from 'react-redux';
import AuthGuard from '../components/AuthGuard';

const mapStateToProps = (state) => {
  return {
    isAuthenticated: isAuthenticated(state),
  };
};

export default connect(
  mapStateToProps,
  null,
)(AuthGuard);
