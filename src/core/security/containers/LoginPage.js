import React from 'react';
import { connect } from 'react-redux';
import { login, loginSuccess } from '../state/actions/loginActions';
import LoginForm from '../components/LoginForm';
import { bindActionCreators } from 'redux';
import { isAuthenticated } from '../state/selectors';
import { register } from '../state/actions/registerActions';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';
import withBase from 'core/components/withBase';

const successResponseGoogle = (response, register) => {
  console.log('successResponseGoogle', response);
  const { profileObj } = response;
  const { email, name, googleId } = profileObj;
  const password = 'test@123'; // since there is no back end support to verify the google api token, this work around
  register({ name, email, password, role: 'user', googleId });
};

const failureResponseGoogle = (response) => {
  console.log('failureResponseGoogle', response);
};

const API_ENTITIES = ['LOGIN'];

export const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: bindActionCreators(login, dispatch),
    // onLoginSuccess: bindActionCreators(loginSuccess, dispatch),
    successResponseGoogle,
    failureResponseGoogle,
    register: bindActionCreators(register, dispatch),
  };
};

const mapStateToProps = (state, ownProps) => {
  // const userDetails = (state) => state.userDetails;
  return {
    // userDetails,
    isAuthenticated: isAuthenticated(state),
    location: ownProps.location,
    loading: getAPILoadingStatus(API_ENTITIES)(state),
    error: getAPIErrorMessage(API_ENTITIES)(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withBase(LoginForm));
