import React from 'react';
import { connect } from 'react-redux';

import RegisterForm from '../components/RegisterForm';
import { bindActionCreators } from 'redux';
import { isAuthenticated } from '../state/selectors';
import { register } from '../state/actions/registerActions';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';
import withBase from 'core/components/withBase';

const mapDispatchToProps = (dispatch) => {
  return {
    onRegister: bindActionCreators(register, dispatch),
  };
};

const API_ENTITIES = ['REGISTER'];

const mapStateToProps = (state) => {
  return {
    isAuthenticated: isAuthenticated(state),
    loading: getAPILoadingStatus(API_ENTITIES)(state),
    error: getAPIErrorMessage(API_ENTITIES)(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withBase(RegisterForm));
