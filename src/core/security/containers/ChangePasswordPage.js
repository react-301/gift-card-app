import React from 'react';
import { connect } from 'react-redux';
import ChangePasswordDialog from '../components/ChangePasswordForm/index';

const mapStateToProps = null;

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  null,
)(ChangePasswordDialog);
