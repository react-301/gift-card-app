import { mapDispatchToProps } from './LoginPage';
import * as actions from '../state/actions/loginActions';

describe('login page spec test suite', () => {
  it('mapDispatch test', () => {
    // mock function
    const dispatchMock = jest.fn();
    const props = mapDispatchToProps(dispatchMock);

    props.onLogin({ email: 'test', password: 'test' });

    expect(dispatchMock).toBeCalled();
    expect(dispatchMock).toHaveBeenCalledTimes(1);
    expect(dispatchMock).toHaveBeenCalledWith(
      actions.login({ email: 'test', password: 'test' }),
    );
  });
});
