import { Redirect, Route } from 'react-router-dom';
import React from 'react';

const AuthGuard = ({ component: Component, isAuthenticated, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isAuthenticated)
          return (
            <Redirect
              to={{ pathname: '/login', state: { from: props.location } }}
            />
          );

        return <Component {...props} />;
      }}
    />
  );
};
export default React.memo(AuthGuard);
