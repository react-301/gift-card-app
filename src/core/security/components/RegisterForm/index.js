import React, { Component } from 'react';
import { Formik } from 'formik';
import withStyles from '@material-ui/core/styles/withStyles';
import Form from './Form';
import Paper from '@material-ui/core/Paper';
import * as Yup from 'yup';
import { Redirect, Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { getTranslatedMessage } from 'core/i18n/util';
import * as constants from '../../constants';
import { FormattedMessage } from 'react-intl';
//import FormattedMessage from 'core/i18n/components/TranslateMessage';

const styles = (theme) => ({
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 5}px ${theme.spacing.unit * 5}px ${theme
      .spacing.unit * 5}px`,
  },
  container: {
    maxWidth: '1000px',
    margin: '0 auto',
  },
});

const validationSchema = Yup.object({
  name: Yup.string(constants.ENTER_NAME).required(
    constants.ERROR_NAME_REQUIRED,
  ),
  email: Yup.string(constants.ENTER_EMAIL)
    .email(constants.ERROR_EMAIL_INVALID)
    .required(constants.ERROR_EMAIL_REQUIRED),
  password: Yup.string(constants.ENTER_PASSWORD)
    .min(8, constants.ERROR_PASSWORD_INVALID)
    .required(constants.ERROR_PASSWORD_REQUIRED),
  confirmPassword: Yup.string(constants.ENTER_CONFIRM_PASSWORD)
    .required(constants.ERROR_CONFIRM_PASSWORD_REQUIRED)
    .oneOf([Yup.ref('password')], constants.ERROR_CONFIRM_PASSWORD_MISMATCH),
});

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, onRegister, isAuthenticated } = this.props;
    if (isAuthenticated) return <Redirect to="/" />;
    const values = { name: '', email: '', confirmPassword: '', password: '' };
    return (
      <React.Fragment>
        <div className={classes.container}>
          <Paper elevation={1} className={classes.paper}>
            <h1>
              <FormattedMessage
                id="register.title"
                defaultMessage="Register"
                description="Register header"
              />
              {/* {getTranslatedMessage({
                id: 'register.title',
                defaultMessage: 'Register',
                description: 'Register header',
              })} */}
            </h1>
            <Formik
              render={(props) => <Form {...props} />}
              initialValues={values}
              validationSchema={validationSchema}
              onSubmit={(values, { setSubmitting }) => {
                const { name, email, password } = values;
                onRegister({ name, email, password, role: 'user' });
                setSubmitting(false);
              }}
            />
            <Typography variant="caption" gutterBottom>
              <FormattedMessage
                id="register.alreadyHavingAccount"
                defaultMessage="Already have an account?"
                description="Label for Already have an account?"
              />
              <Link to="login">
                <FormattedMessage
                  id="login.title"
                  defaultMessage="Login"
                  description="Login text"
                />
              </Link>
            </Typography>
          </Paper>
        </div>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(RegisterForm);
