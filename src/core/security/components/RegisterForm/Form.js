import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import NameIcon from '@material-ui/icons/SupervisorAccount';
import LockIcon from '@material-ui/icons/Lock';
import EmailIcon from '@material-ui/icons/Email';
import { withStyles } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';
const styles = {
  vericalAlign: {
    margin: '20px',
  },
};
const Form = (props) => {
  const {
    values: { name, email, password, confirmPassword },
    errors,
    touched,
    handleSubmit,
    handleChange,
    isValid,
    setFieldTouched,
    classes,
  } = props;

  const change = (name, e) => {
    e.persist();
    handleChange(e);
    setFieldTouched(name, true, false);
  };

  return (
    <form onSubmit={handleSubmit}>
      <FormattedMessage
        id="register.name"
        defaultMessage="Name"
        description="Name label in Register form"
      >
        {(lableText) => (
          <TextField
            className={classes.verticalAlign}
            id="name"
            name="name"
            helperText={touched.name ? errors.name : ''}
            error={touched.name && Boolean(errors.name)}
            label={lableText}
            value={name}
            onChange={change.bind(null, 'name')}
            fullWidth
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <NameIcon />
                </InputAdornment>
              ),
            }}
          />
        )}
      </FormattedMessage>

      <FormattedMessage
        id="register.email"
        defaultMessage="Email"
        description="Email label in Register form"
      >
        {(lableText) => (
          <TextField
            className={classes.verticalAlign}
            id="email"
            name="email"
            helperText={touched.email ? errors.email : ''}
            error={touched.email && Boolean(errors.email)}
            label={lableText}
            fullWidth
            value={email}
            onChange={change.bind(null, 'email')}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EmailIcon />
                </InputAdornment>
              ),
            }}
          />
        )}
      </FormattedMessage>

      <FormattedMessage
        id="register.password"
        defaultMessage="Password"
        description="Password label in Register form"
      >
        {(lableText) => (
          <TextField
            className={classes.verticalAlign}
            id="password"
            name="password"
            helperText={touched.password ? errors.password : ''}
            error={touched.password && Boolean(errors.password)}
            label={lableText}
            fullWidth
            type="password"
            value={password}
            onChange={change.bind(null, 'password')}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockIcon />
                </InputAdornment>
              ),
            }}
          />
        )}
      </FormattedMessage>

      <FormattedMessage
        id="register.confirmPassword"
        defaultMessage="Confirm Password"
        description="Confirm Password label in Register form"
      >
        {(lableText) => (
          <TextField
            className={classes.verticalAlign}
            id="confirmPassword"
            name="confirmPassword"
            helperText={touched.confirmPassword ? errors.confirmPassword : ''}
            error={touched.confirmPassword && Boolean(errors.confirmPassword)}
            label={lableText}
            fullWidth
            type="password"
            value={confirmPassword}
            onChange={change.bind(null, 'confirmPassword')}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockIcon />
                </InputAdornment>
              ),
            }}
          />
        )}
      </FormattedMessage>
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        disabled={!isValid}
      >
        <FormattedMessage
          id="register.submit"
          defaultMessage="Submit"
          description="Submit label in Register form"
        />
      </Button>
    </form>
  );
};
export default withStyles(styles)(Form);
