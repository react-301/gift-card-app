import React from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { Formik } from 'formik';
import PropTypes from 'prop-types';
import { GoogleLogin } from 'react-google-login';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage } from 'react-intl';
import { ERROR_EMAIL_INVALID, ERROR_EMAIL_REQUIRED } from '../constants';

const loginBackground = require('../../../assets/images/loginBackground.jpg');

const styles = (theme) => ({
	root: {
		display: 'flex',
		flexDirection: 'column',
		'& > *+*': {
			marginTop: theme.spacing.unit
		}
	},
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: '10%'
		}
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit
	},
	submit: {
		marginTop: theme.spacing.unit * 3
	},
	loginPage: {
		backgroundImage: `url(${loginBackground})`,
		backgroundSize: 'cover',
		height: '80vh',
		paddingTop: '1px'
	}
});

const GOOGLE_CLIENT_ID = process.env.REACT_APP_GOOGLE_CLIENT_ID;

const LoginForm = ({
	onLogin,
	location,
	isAuthenticated,
	failureResponseGoogle,
	successResponseGoogle,
	register,
	classes
}) => {
	const { from } = location.state || { from: { pathname: '/' } };
	if (isAuthenticated) return <Redirect to={from} />;
	// console.log('LOGIN', userDetails);
	return (
		<div className={classes.loginPage}>
			<Formik
				initialValues={{ email: 'test@email.com', password: 'test' }}
				validate={(values) => {
					let errors = {};
					if (!values.email) {
						errors.email = ERROR_EMAIL_REQUIRED;
					} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
						errors.email = ERROR_EMAIL_INVALID;
					}
					return errors;
				}}
				onSubmit={(values, { setSubmitting }) => {
					const { email, password } = values;
					onLogin({ email, password });
					setSubmitting(false);
				}}
			>
				{({
					values,
					errors,
					touched,
					handleChange,
					handleBlur,
					handleSubmit,
					isSubmitting
					/* and other goodies */
				}) => (
					<main className={classes.main}>
						<CssBaseline />
						<Paper className={classes.paper}>
							<Avatar className={classes.avatar}>
								<LockOutlinedIcon />
							</Avatar>

							<Typography component="h1" variant="h5">
								<FormattedMessage id="login.title" defaultMessage="Login" description="Login text" />
							</Typography>
							<form onSubmit={handleSubmit} className={classes.form}>
								<FormControl margin="normal" required fullWidth>
									<InputLabel htmlFor="email">
										<FormattedMessage
											id="login.emailAddress"
											defaultMessage="Email Address"
											description="Login Email Address Label"
										/>
									</InputLabel>
									<Input
										id="email"
										name="email"
										autoComplete="email"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.email}
										autoFocus
										type="email"
									/>
								</FormControl>
								{/* <input
                  type="email"
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                /> */}
								{errors.email && touched.email && errors.email}
								<FormControl margin="normal" required fullWidth>
									<InputLabel htmlFor="password">
										<FormattedMessage
											id="login.password"
											defaultMessage="Password"
											description="Login  Password Label"
										/>
									</InputLabel>
									<Input
										name="password"
										type="password"
										id="password"
										autoComplete="current-password"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.password}
									/>
								</FormControl>
								{/* <input
                  type="password"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                /> */}
								{errors.password && touched.password && errors.password}
								<Button
									type="submit"
									disabled={isSubmitting}
									fullWidth
									variant="contained"
									color="primary"
									className={classes.submit}
								>
									<FormattedMessage
										id="login.submit"
										defaultMessage="Submit"
										description="Login  Submit button Label"
									/>

									{/* {TranslateMessage({
                    id: 'login.submit',
                    defaultMessage: 'Submit',
                    description: 'Login  Submit button Label',
                  })} */}
								</Button>
							</form>
							<br />
							<div className={classes.root}>
								<FormattedMessage
									id="login.google"
									defaultMessage="Login using Google"
									description="Google login button text"
								>
									{(buttonText) => (
										<GoogleLogin
											clientId={GOOGLE_CLIENT_ID}
											buttonText={buttonText}
											onSuccess={(response) => successResponseGoogle(response, register)}
											onFailure={failureResponseGoogle}
											cookiePolicy={'single_host_origin'}
										/>
									)}
								</FormattedMessage>
							</div>
							<br />
							<Grid container>
								<Grid item xs>
									<Link to="/forgotpassword" variant="body2">
										Forgot password?
									</Link>
								</Grid>
								<Grid item>
									<Typography variant="caption" gutterBottom>
										<FormattedMessage
											id="login.dontHaveAccount"
											defaultMessage="Don't have an account?"
											description="Login  Don't have an account Label"
										/>
										<Link to="/register">
											<FormattedMessage
												id="login.register"
												defaultMessage="Register"
												description="Login  Register Label"
											/>
										</Link>
									</Typography>
								</Grid>
							</Grid>
						</Paper>
					</main>
				)}
			</Formik>
		</div>
	);
};

LoginForm.propTypes = {
	onLogin: PropTypes.func.isRequired,
	history: PropTypes.any.isRequired
};

export default React.memo(withStyles(styles)(withRouter(LoginForm)));
