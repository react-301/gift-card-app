import { createSelector } from 'reselect';

import { isEmpty } from 'lodash';

const auth = (state) => state.auth;
const userDetails = (state) => state.userDetails;

// eslint-disable-next-line import/prefer-default-export
export const getAuthToken = createSelector(auth, (auth) => {
	console.log('in getAuthToken selectors');
	return auth.token;
});

export const isAuthenticated = createSelector(auth, (auth) => {
	return !isEmpty(auth.token);
});

export const populateUser = createSelector(userDetails, (userDetails) => {
	return userDetails;
});
