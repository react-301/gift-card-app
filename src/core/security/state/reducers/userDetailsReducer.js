import { USERDETAILS_SUCCESS } from '../actionTypes/userDetailsActionType';
// import { REGISTER_SUCCESS } from '../actionTypes/registerActionTypes';

const userInitialState = {
	email: '',
	name: '',
	role: '',
	id: null
};

export default function(state = userInitialState, action) {
	switch (action.type) {
		case USERDETAILS_SUCCESS:
			return { ...state, ...action.payload };
		default:
			return state;
	}
}
