import authReducer from './loginReducer';
import * as ActionTypes from '../actionTypes/loginActionTypes';

describe('auth Reducer test suite ', () => {
  it('should have auth reducer default state ', () => {
    // toEqual deep comapre
    expect(authReducer(undefined, { type: 'NOT THERE' })).toEqual({
      token: '',
      userEmail: '',
    });
  });

  it('should set the token on login success', () => {
    // toEqual deep comapre
    const action = {
      type: ActionTypes.LOGIN_SUCCESS,
      payload: {
        token: 'test',
        userEmail: 'test',
      },
    };
    expect(
      authReducer(
        {
          token: '',
          userEmail: '',
        },
        action,
      ),
    ).toEqual({
      token: 'test',
      userEmail: 'test',
    });
  });
});
