import { LOGIN_SUCCESS } from '../actionTypes/loginActionTypes';
import { REGISTER_SUCCESS } from '../actionTypes/registerActionTypes';

const loginInitialState = {
  token: '',
  userEmail: '',
};

export default function(state = loginInitialState, action) {
  switch (action.type) {
    // case LOGIN_REQUEST:
    // 	return { ...state, loading: true };
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
