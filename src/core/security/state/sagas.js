import { take, put, call, race, cancelled, takeLatest } from 'redux-saga/effects';
import { LOGIN_REQUEST, LOGOUT_REQUEST, LOGIN_FAILURE } from './actionTypes/loginActionTypes';
import { authenticate, register, getUserByEmail } from '../api/services';
import { loginSuccess, loginFail } from './actions/loginActions';
import { REGISTER_REQUEST } from './actionTypes/registerActionTypes';
import { registerFail, registerSuccess } from './actions/registerActions';
import { getUserDetailsSuccess } from './actions/userDetailsAction';

export function* watchLogin() {
	while (true) {
		console.log('waiting for login request');
		// throw new Error('error in watchLogin');
		const { payload } = yield take(LOGIN_REQUEST);
		const { email, password } = payload;
		yield race({
			authorize: call(authenticateUser, email, password),
			cancel: take(LOGOUT_REQUEST)
		});
	}
}

function* authenticateUser(email, password) {
	try {
		console.log('in authenticateUser');
		const { data } = yield call(authenticate, email, password);

		const { accessToken } = data;
		console.log('after call to authenticateUser');

		yield put(loginSuccess({ token: accessToken, userEmail: email }));
		const { data: userData } = yield call(getUserByEmail, email);
		const user = userData[0];
		yield put(getUserDetailsSuccess(user));
	} catch (error) {
		yield put(loginFail({ error }));
	} finally {
		if (yield cancelled()) {
			yield put(LOGIN_FAILURE, 'Login is cancelled by the user');
		}
	}
}

function* populateUser(email) {
	try {
		const { data } = yield call(getUserByEmail, email);
		const { id, name } = data[0];
	} catch (error) {
		yield put(loginFail({ error }));
	}
}
export function* watchRegister() {
	console.log('waiting for register request');
	// throw new Error('error in watchLogin');
	yield takeLatest(REGISTER_REQUEST, registerUser);
}

function* registerUser(action) {
	try {
		const { name, email, password, role, googleId } = action.payload;
		const { data } = yield call(register, {
			name,
			email,
			password,
			role,
			googleId
		});
		const { accessToken } = data;

		//const { data: userData } = yield call(getUserByEmail, email);
		//const { id } = userData[0];

		yield put(registerSuccess({ token: accessToken, userEmail: email }));
	} catch (error) {
		yield put(registerFail({ error: error.response }));
	}
}
