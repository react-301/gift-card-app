import { USERDETAILS_REQUEST, USERDETAILS_SUCCESS, USERDETAILS_FAILURE } from '../actionTypes/userDetailsActionType';

export function getUserDetails({ email, password }) {
	return { type: USERDETAILS_REQUEST, payload: { email, password } };
}

export function getUserDetailsSuccess(payload) {
	return { type: USERDETAILS_SUCCESS, payload: { ...payload } };
}

export function getUserDetailsFail({ error }) {
	return { type: USERDETAILS_FAILURE, payload: { error } };
}
