import {
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
} from '../actionTypes/registerActionTypes';

export function register({ name, email, password, role, googleId }) {
  return {
    type: REGISTER_REQUEST,
    payload: { name, email, password, role, googleId },
  };
}

export function registerSuccess(payload) {
  return { type: REGISTER_SUCCESS, payload: { ...payload } };
}

export function registerFail({ error }) {
  return { type: REGISTER_FAILURE, payload: { error } };
}
