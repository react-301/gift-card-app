import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from '../actionTypes/loginActionTypes';

export function login({ email, password }) {
  return { type: LOGIN_REQUEST, payload: { email, password } };
}

export function loginSuccess(payload) {
  return { type: LOGIN_SUCCESS, payload: { ...payload } };
}

export function loginFail({ error }) {
  return { type: LOGIN_FAILURE, payload: { error } };
}
