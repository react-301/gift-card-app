import { combineReducers } from 'redux';
import loading from './loadingReducers';
import error from './errorReducer';

import auth from '../../security/state/reducers/loginReducer';
import userDetails from '../../../core/security/state/reducers/userDetailsReducer';

const api = combineReducers({
	loading,
	error
});

const coreReducers = { auth, userDetails, api };

export default coreReducers;
