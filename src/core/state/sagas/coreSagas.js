import { watchLogin, watchRegister } from '../../security/state/sagas';

export const coreSagas = [watchLogin, watchRegister];
