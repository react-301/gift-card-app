/* eslint-disable no-console */
import { spawn, fork, delay, call } from 'redux-saga/effects';

const sagaRestartAttempts = 3;
//Saga wrapper: Keeps the Saga Alive restarts on crash.
const makeRestartable = (saga) => {
  let restartAttempts = sagaRestartAttempts; //TODO: pick it from config
  return function*() {
    yield spawn(function*() {
      while (restartAttempts !== 0) {
        --restartAttempts;
        try {
          yield call(saga);
        } catch (e) {
          yield null;
        }
        yield delay(1000);
      }
    });
  };
};
//Saga wrapper: Used to launch multiple sagas at once.
const composeParallel = (sagas, params) => {
  return function*() {
    yield call(function*() {
      for (let index = 0; index < sagas.length; index++) {
        yield fork(sagas[index], ...params[index]);
      }
    });
  };
};
//Saga wrapper: Makes the Saga(s) independent of the host saga.
const composeDetached = (sagas, params) => {
  return function*() {
    yield spawn(function*() {
      for (let index = 0; index < sagas.length; index++) {
        yield spawn(sagas[index], ...params[index]);
      }
    });
  };
};

export { makeRestartable, composeParallel, composeDetached };
