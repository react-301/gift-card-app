import React from 'react';
import { FormattedMessage } from 'react-intl';
import { injectIntl, intlShape } from 'react-intl';

const TranslateMessage = (props) => {
  const { intl, type, children } = props;
  const { id, defaultMessage, description } = props;
  if (type === 'text') {
    const message = intl.formatMessage({ id, defaultMessage, description });
    return message;
  }
  if (typeof children === 'function') {
    const message = intl.formatMessage({ id, defaultMessage, description });
    //return message;
    return children(message);
  }

  return <FormattedMessage {...props} />;
};

export default injectIntl(TranslateMessage);
