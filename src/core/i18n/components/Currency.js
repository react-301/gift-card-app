import { FormattedNumber } from 'react-intl';
import React from 'react';

const Currency = (props) => {
  const currency = props.currency;
  const minimum = props.minimumFractionDigits || 2;
  const maximum = props.maximumFractionDigits || 2;
  return (
    <FormattedNumber
      value={props.amount}
      style="currency"
      currency={currency}
      minimumFractionDigits={minimum}
      maximumFractionDigits={maximum}
    />
  );
};

export default Currency;
