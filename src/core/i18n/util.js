import TranslateMessage from './components/TranslateMessage';
import React from 'react';

export function getTranslatedMessage(props) {
  return <TranslateMessage {...props} type="text" />;
}
