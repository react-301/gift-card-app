import { GET_BRAND_SUCCESS } from './actionType';

export default function(state = [ {} ], action) {
	switch (action.type) {
		case GET_BRAND_SUCCESS:
			return { ...action.payload };
		default:
			return state;
	}
}
