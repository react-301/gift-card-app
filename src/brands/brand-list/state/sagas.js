import { takeLatest, put, call, race, cancelled } from 'redux-saga/effects';
import { GET_BRAND_REQUEST } from './actionType';
import { fetchProductsBrand } from '../../../products/product-list/api/services';
import { getBrandSuccess, getBrandFailure } from './actions.js';

export default function* watchGetProductsBrand() {
	yield takeLatest(GET_BRAND_REQUEST, getBrand);
}

function* getBrand(action) {
	try {
		console.log('before fetching Brand');
		const { data } = yield call(fetchProductsBrand);
		console.log('after fetching Brand', data);
		yield put(getBrandSuccess(data));
	} catch (error) {
		yield put(getBrandFailure(error));
	}
}
