import { GET_BRAND_REQUEST, GET_BRAND_SUCCESS, GET_BRAND_FAILURE } from './actionType';

export function getBrand() {
	return { type: GET_BRAND_REQUEST };
}

export function getBrandSuccess(productsBrand) {
	return { type: GET_BRAND_SUCCESS, payload: { ...productsBrand } };
}

export function getBrandFailure(error) {
	return { type: GET_BRAND_FAILURE, payload: { error } };
}
