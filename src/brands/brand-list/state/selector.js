import { createSelector } from 'reselect';

const loadProductsBrand = (state) => state.productsBrand;

export default loadProductsBrand;
