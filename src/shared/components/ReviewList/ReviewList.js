import React from 'react';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';

const styles = (theme) => ({
	avatar: {
		margin: 10
	}
});

const ReviewList = ({ productReviews }) => {
	console.log('PR', productReviews);
	// const { classes } = props;
	return productReviews.map((item) => {
		return (
			<Grid container>
				<Grid xs={12} sm={12} md={2} lg={2}>
					<Avatar>{item.userId}</Avatar>
				</Grid>
				<Grid xs={12} sm={12} md={10} lg={10}>
					{item.comments}
				</Grid>
			</Grid>
		);
	});
};

export default React.memo(ReviewList);
