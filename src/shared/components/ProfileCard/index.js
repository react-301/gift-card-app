import React,{useState,useEffect} from 'react';
import './index.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Icon from "@material-ui/core/Icon";
import Fab from '@material-ui/core/Fab';
import TextField from '@material-ui/core/TextField';

const styles = (theme) => ({
    image: {
        width: "100%"
    },
    fab: {
        margin: theme.spacing.unit,
    },
    textField: {
        marginTop: "8px",
        marginBottom: "0px",
        margin: "4px 0px 5px"
    },
    fabGreen:{
        margin:theme.spacing.unit,
        backgroundColor:"#32cd32",
        '&:hover': {
           backgroundColor: "#00ff00"
         },
    }

});
const ProfileCard = ({ classes }) => {
    const [edit, setEdit] = useState(false);
    return (


        <div class="card-profile">
            <div class="card-profile_visual">
                <img className={classes.image} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYERCqZ1DunHMgZ1S4ict2pPvSh5BfmBY9yjbeRZTal28yr2WPmg" />
            </div>

            <div class="card-profile_user-infos">

{   edit?
                <Fab color="primary" aria-label="Edit" className={classes.fabGreen} onClick={()=>setEdit(false)}>
                    <Icon>done</Icon>
                </Fab>
                :
                <Fab color="secondary" aria-label="Edit" className={classes.fab} onClick={()=>setEdit(true)}>
                    <Icon>edit_icon</Icon>
                </Fab>}


            </div>

            <div class="stats-holder">
                {!edit? <div className="div1"><p>Name</p>
                    <p>Email</p>
                    <p>Mobile</p>
                    <p>Gender</p></div> :
                    <div>
                        <TextField
                            id="standard-bare"
                            className={classes.textField}
                            defaultValue="Bare"
                            margin="normal" />
                        <TextField
                            id="standard-bare"
                            className={classes.textField}
                            defaultValue="Bare"
                            margin="normal" />
                        <TextField
                            id="standard-bare"
                            className={classes.textField}
                            defaultValue="Bare"
                            margin="normal"
                        />
                        <TextField
                            id="standard-bare"
                            className={classes.textField}
                            defaultValue="Bare"
                            margin="normal" />
                    </div>}

            </div>

        </div>

    );

}

// ProfileCard.propTypes = {
//     classes: PropTypes.object.isRequired,
// };

export default withStyles(styles)(React.memo(ProfileCard));