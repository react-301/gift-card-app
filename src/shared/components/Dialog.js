import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

function ProductDialog(props) {
    const { classes, onFilter } = props;

    const [firstField, setfirstField] = useState(props.data.name);
    const [secondField, setsecondField] = useState(props.data.brand);
    const [thirdField, setthirdField] = useState(props.data.desc);
    const [fourthField, setfourthField] = useState(props.data.imageUrl);
    const [fifthField, setfifthField] = useState(props.data.points);
    const [sixthField, setsixthField] = useState(props.data.expiryDays);
    const [seventhField, setseventhField] = useState(props.data.categoryId);
    const [eightField, seteightField] = useState(props.data.productId);
    const [ninthField, setninthField] = useState(props.data.rating);


    const handleProduct = (data) => {
        console.log('hiii', data)
        props.handleEditData(data);
    };
    return (
        <div>
            {  console.log("Value", props.data)
  }
            <Dialog open={props.open} onClose={() => props.handleClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add {props.type}</DialogTitle>
                <div>
            <DialogContent>
                <TextField
                value={firstField}
                autoFocus
                margin="dense"
                id="name"
                label="Name"
                type="text"
                fullWidth
                required
                onChange={(e) => { setfirstField(e.target.value) }}
            />
             <TextField
             value={secondField}
                autoFocus
                margin="dense"
                id="brand"
                label="Brand"
                type="text"
                fullWidth
                required
                onChange={(e) => { setsecondField(e.target.value) }}
            />
                <TextField
                value={thirdField}
                    autoFocus
                    margin="dense"
                    id="description"
                    label="Description"
                    type="text"
                    fullWidth
                    required
                    multiline
                    onChange={(e) => { setthirdField(e.target.value) }}
                />
                 <TextField
                 value={fourthField}
                    autoFocus
                    margin="dense"
                    id="url"
                    label="Image URL"
                    type="text"
                    fullWidth
                    required
                    onChange={(e) => { setfourthField(e.target.value) }}
                />
                <TextField
                value={fifthField}
                    autoFocus
                    margin="dense"
                    id="Points"
                    label="Points"
                    type="number"
                    fullWidth
                    required
                    min="0"
                    onChange={(e) => { setfifthField(e.target.value) }}
                />
                 <TextField
                 value={sixthField}
                    autoFocus
                    margin="dense"
                    id="text"
                    label="Expiry Days"
                    type="text"
                    fullWidth
                    required
                    onChange={(e) => { setsixthField(e.target.value) }}
                />
                <TextField
                value={seventhField}
                autoFocus
                margin="dense"
                id="category"
                label="category ID"
                type="number"
                fullWidth
                required
                min="0"
                onChange={(e) => { setseventhField(parseInt(e.target.value));console.log(seventhField,typeof(seventhField)) }}
            />
             <TextField
             value={eightField}
                autoFocus
                margin="dense"
                id="productId"
                label="Product id"
                type="text"
                fullWidth
                required
                onChange={(e) => { seteightField(e.target.value) }}
            />
              <TextField
              value={ninthField}
                autoFocus
                margin="dense"
                id="rating"
                label="Rating"
                type="number"
                fullWidth
                required
                onChange={(e) => { setninthField(e.target.value) }}
            />

            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.handleClose()} color="primary">
                    Cancel
          </Button>
                <Button onClick={() =>{handleProduct({name:firstField,brand:secondField,desc:thirdField,imageUrl:fourthField,points:fifthField,expiryDays:sixthField,categoryId:seventhField,id:eightField,rating:ninthField}); props.handleClose()}} color="primary">
                    Add
          </Button>
            </DialogActions>
        </div>
            </Dialog>
        </div>
    );
}

export default ProductDialog;