import React, { useState } from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const styles = (theme) => ({
	root: {
		padding: '12px 8px',
		display: 'flex',
		alignItems: 'center',
		width: '100%'
	},
	page: {
		display: 'inline-flex'
	},
	orderStyle: {
		textDecoration: 'none'
	}
});

const Pagination = (props, ownProps) => {
	const { classes, onPageChange } = props;
	const [ pageNo, setPage ] = useState(1);

	const handlePage = (item) => {
		switch (item.name) {
			case 'firstPage':
				setPage(item.value);
				onPageChange({ name: 'pagination', pageNo: item.value });
				break;
			case 'prevPage':
				setPage(item.value);
				onPageChange({ name: 'pagination', pageNo: item.value });
				break;
			case 'nextPage':
				setPage(item.value);
				onPageChange({ name: 'pagination', pageNo: item.value });
				break;
			case 'lastPage':
				setPage(item.value);
				onPageChange({ name: 'pagination', pageNo: item.value });
				break;
			default:
				break;
		}
		console.log(item.value, pageNo);
		// onPageChange({ name: 'pagination', pageNo: pageNo });
	};
	console.log('OWN', ownProps, props.totalItems, pageNo);

	return (
		<div className={classes.root}>
			<p>
				Page {pageNo} of {Math.ceil(props.totalItems / 31)}
			</p>
			<div>
				<IconButton disabled={pageNo === 1} onClick={() => handlePage({ name: 'firstPage', value: 1 })}>
					<FirstPageIcon />
				</IconButton>
				<IconButton
					disabled={pageNo === 1}
					onClick={() =>
						pageNo > 1
							? handlePage({ name: 'prevPage', value: pageNo - 1 })
							: handlePage({ name: 'prevPage', value: 1 })}
				>
					<KeyboardArrowLeft />
				</IconButton>
				<IconButton
					disabled={pageNo === Math.ceil(props.totalItems / 31)}
					onClick={() =>
						pageNo < Math.ceil(props.totalItems / 31)
							? handlePage({ name: 'nextPage', value: pageNo + 1 })
							: handlePage({ name: 'nextPage', value: pageNo })}
				>
					<KeyboardArrowRight />
				</IconButton>
				<IconButton
					disabled={pageNo === Math.ceil(props.totalItems / 31)}
					onClick={() => handlePage({ name: 'lastPage', value: Math.ceil(props.totalItems / 31) })}
				>
					<LastPageIcon />
				</IconButton>
			</div>
		</div>
	);
};

export default withStyles(styles)(Pagination);
