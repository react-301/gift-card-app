import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
// import Paper from '@material-ui/core/Paper';
// import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ArrowDownward from '@material-ui/icons/ArrowDownward';

const styles = (theme) => ({
	root: {
		flexGrow: 1,
		marginBottom: '1%'
	},
	margin: {
		margin: theme.spacing.unit
	}
});

function SortGrid(props) {
	const { classes, onSort } = props;

	const handleSort = (value) => {
		onSort(value);
	};

	return (
		<React.Fragment>
			<Fab
				variant="extended"
				size="medium"
				color="primary"
				aria-label="sortbypoint"
				className={classes.margin}
				onClick={() => handleSort({ name: 'points', value: 'asc' })}
			>
				Points: Low-High
				<ArrowUpward />
			</Fab>
			<Fab
				variant="extended"
				size="medium"
				color="primary"
				aria-label="sortbypoint"
				className={classes.margin}
				onClick={() => handleSort({ name: 'points', value: 'desc' })}
			>
				Points: High-Low
				<ArrowDownward />
			</Fab>
			<Fab
				variant="extended"
				size="medium"
				color="primary"
				aria-label="sortbyasc"
				className={classes.margin}
				onClick={() => handleSort({ name: 'id', value: 'asc' })}
			>
				<ArrowUpward />
				A-Z
			</Fab>
			<Fab
				variant="extended"
				size="medium"
				color="primary"
				aria-label="sortbydecs"
				className={classes.margin}
				onClick={() => handleSort({ name: 'Id', value: 'desc' })}
			>
				<ArrowDownward />
				Z-A
			</Fab>
		</React.Fragment>
	);
}

SortGrid.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SortGrid);
