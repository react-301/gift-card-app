import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import StarRate from '@material-ui/icons/StarRate';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import { postProduct } from '../../../shared/api/services';
import SnackBar from '../Snackbar/SnackBar';
import Icon from '@material-ui/core/Icon';
import ProductDialog from '../Dialog';

const styles = (theme) => ({
	card: {
		display: 'flex',
		height: 221
	},
	details: {
		display: 'flex',
		flexDirection: 'column',
		width: 151
	},
	content: {
		flex: '1 0 auto',
		cursor: 'pointer'
	},
	cover: {
		width: 224,
		cursor: 'pointer'
	},
	controls: {
		display: 'flex',
		alignItems: 'center',
		paddingLeft: theme.spacing.unit,
		paddingBottom: theme.spacing.unit
	},
	shareIcon: {
		color: '#008836'
	},
	rating: {
		border: '1px solid #a9a9a9',
		paddingLeft: '5%',
		paddingRight: '5%',
		width: 'max-content',
		display: 'inline-flex',
		height: '27px'
	}
});

function GiftControlCard(props) {
	const { classes, theme } = props;
	const [ openSnack, setOpenSnack ] = useState(false);
	const [ onComplete, setOnComplete ] = React.useState(false);
	const [ open, setOpen ] = React.useState(false);
	const handleEditData = (filterParams) => {
		props.editData(filterParams);
		console.log('edit');
	};
	const handleDeleteData = (id) => {
		props.deleteData(id);
		console.log('delete');
	};

	// console.log('IN CARD', props);
	const handleProductDetail = (productId) => {
		// console.log(productId);
		props.history.push(`/products/${productId}`);
	};
	const handleCart = (product, isLogged, userDetails) => {
		// console.log(isLogged);
		if (!isLogged) {
			props.history.push('/login');
		} else {
			// product['userId'] = 1;
			if (product['userId'] === undefined || product['userId'] !== userDetails.id) {
				product['userId'] = userDetails.id;
				postProduct(product)
					.then((res) => {
						if (res.status === 201 || res.statusText === 'Created') {
							setOpenSnack(true);
						} else {
							alert('Post Error');
						}
					})
					.catch((e) => {
						alert('Item added already');
					});
			}
		}
	};
	return (
		<Card className={classes.card}>
			<CardMedia
				className={classes.cover}
				image={props.value.imageUrl !== undefined || props.value.imageUrl !== null ? props.value.imageUrl : ''}
				onClick={() => {
					props.role == 'user' ? handleProductDetail(props.value.id) : console.log('a');
				}}
			/>
			<div className={classes.details}>
				<CardContent className={classes.content} onClick={() => handleProductDetail(props.value.id)}>
					<Typography variant="button" gutterBottom>
						{props.value.name}
					</Typography>
					<Typography variant="subtitle1" color="textSecondary">
						<div className={classes.rating}>
							<div>{props.value.rating}</div>
							<div>
								<StarRate />
							</div>
						</div>
					</Typography>
					<br />
					<Typography variant="subtitle1" color="textSecondary">
						Points: {props.value.points}
					</Typography>
				</CardContent>
				{props.role == 'user' ? (
					<div className={classes.controls}>
						<IconButton aria-label="Add to shopping cart">
							<FavoriteIcon />
						</IconButton>
						<IconButton aria-label="Add to shopping cart">
							<AddShoppingCartIcon
								onClick={() => handleCart(props.value, props.isLogged, props.userDetails)}
							/>
						</IconButton>
						<IconButton className={classes.shareIcon} aria-label="Share">
							<ShareIcon />
						</IconButton>
					</div>
				) : (
					<div className={classes.controls}>
						<IconButton aria-label="Edit" onClick={() => setOpen(true)}>
							<Icon className="icon">edit</Icon>
						</IconButton>
						<IconButton aria-label="Delete" onClick={() => handleDeleteData(props.value.id)}>
							<Icon className="icon">delete</Icon>
						</IconButton>
					</div>
				)}
			</div>
			<SnackBar
				onComplete={onComplete}
				completeClose={() => setOnComplete(true)}
				open={openSnack}
				close={() => setOpenSnack(false)}
				msg="Item added to your cart!"
			/>

			{open ? (
				<ProductDialog
					handleClose={() => {
						setOpen(false);
					}}
					open={open}
					data={props.value}
					handleEditData={(e) => {
						handleEditData(e);
					}}
				/>
			) : (
				<div />
			)}
		</Card>
	);
}

GiftControlCard.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(withRouter(GiftControlCard));
