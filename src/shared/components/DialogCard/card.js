import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import withMobileDialog from '@material-ui/core/withMobileDialog';

const emailjs = require('emailjs-com');

let isSent = false;

const styles = {
	root: {
		margin: '0px',
		width: '1005'
	}
};

function DialogCard(props) {
	const { fullScreen, classes } = props;
	const [ to_name, setTo_name ] = React.useState(false);
	const [ message, setMessage ] = React.useState(false);

	return (
		<div>
			<Dialog
				fullScreen={fullScreen}
				open={props.open}
				onClose={() => {
					props.close();
				}}
				aria-labelledby="form-dialog-title"
				style={{ width: '100%' }}
			>
				<DialogTitle id="form-dialog-title">Send Card</DialogTitle>
				<DialogContent>
					<TextField
						autoFocus
						margin="dense"
						id="name"
						label="Email Address"
						type="email"
						fullWidth
						onChange={(e) => setTo_name(e.target.value)}
					/>
					<TextField
						autoFocus
						margin="dense"
						id="message"
						label="Add a Message"
						type="text"
						fullWidth
						onChange={(e) => setMessage(e.target.value)}
					/>
				</DialogContent>
				<DialogActions>
					<Button
						color="primary"
						onClick={() => {
							props.close(false);
						}}
					>
						Cancel
					</Button>
					<Button
						onClick={() => {
							var templateParams = {
								to_name: to_name,
								message: message
							};
							emailjs
								.send('gmail', 'template_9Kgs3pRK', templateParams, 'user_OdNPIWjbzC2XprV9mK4sA')
								.then(
									function(response) {
										console.log('SUCCESS!', response.status, response.text);
										props.close(true);
									},
									function(error) {
										console.log('FAILED...', error);
									}
								);

							props.close(isSent);
						}}
						color="primary"
					>
						Send
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}

DialogCard.propTypes = {
	fullScreen: PropTypes.bool.isRequired
};

export default withMobileDialog()(DialogCard);
