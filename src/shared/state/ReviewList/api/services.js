import httpClient from '../../../../core/services/baseService/httpClient';

export function fetchProductReviews(productId) {
	return httpClient.get(`products/${productId}/reviews`);
}
