import { takeLatest, put, call } from 'redux-saga/effects';
import { GET_PRODUCTREVIEWS_REQUEST } from './actionType';
import { fetchProductReviews } from '../api/services';
import { getProductReviewsSuccess, getProductReviewsFailure } from './action';

export default function* watchGetProductReviews() {
	yield takeLatest(GET_PRODUCTREVIEWS_REQUEST, getProductReviews);
}

function* getProductReviews(action) {
	try {
		console.log('before fetch product reviews');
		const { data } = yield call(fetchProductReviews, action.payload.productId);
		console.log('after fetchProduct reviews', data);
		yield put(getProductReviewsSuccess(data));
	} catch (error) {
		yield put(getProductReviewsFailure(error));
	}
}
