import { GET_PRODUCTREVIEWS_SUCCESS } from './actionType';

export default function(state = [ {} ], action) {
	switch (action.type) {
		case GET_PRODUCTREVIEWS_SUCCESS:
			// console.log('SUCCESS', ...action.payload);
			return action.payload;
		default:
			return state;
	}
}
