// import { createSelector } from 'reselect';

const loadProductReviews = (state) => state.productReviews;

export default loadProductReviews;
