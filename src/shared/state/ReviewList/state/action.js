import { GET_PRODUCTREVIEWS_REQUEST, GET_PRODUCTREVIEWS_SUCCESS, GET_PRODUCTREVIEWS_FAILURE } from './actionType';

export function getProductReviews(productId) {
	return { type: GET_PRODUCTREVIEWS_REQUEST, payload: { productId } };
}

export function getProductReviewsSuccess(productReviews) {
	return { type: GET_PRODUCTREVIEWS_SUCCESS, payload: productReviews };
}

export function getProductReviewsFailure(error) {
	return { type: GET_PRODUCTREVIEWS_FAILURE, payload: { error } };
}
