import httpClient from '../../core/services/baseService/httpClient';

export function fetchSortedProducts() {
	return httpClient.get('products/?_sort=id&_order=desc');
}

export function postProduct(product) {
	return httpClient.post('cart', product);
}
export function editProduct(id, productDetails) {
	return httpClient.post(`products/${id}`, productDetails);
}
