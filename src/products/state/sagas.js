import watchGetProduct from '../product-detail/state/sagas';
import watchGetProducts from '../product-list/state/sagas';
import watchGetProductsBrand from '../../brands/brand-list/state/sagas';
import watchGetProductReviews from '../../shared/state/ReviewList/state/saga';
import watchGetCartProducts from '../../products/product-cart/state/sagas';

export const productSagas = [
	watchGetProduct,
	watchGetProducts,
	watchGetProductsBrand,
	watchGetProductReviews,
	watchGetCartProducts
];
