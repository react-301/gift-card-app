import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// var  emailjs =  require('emailjs-com');
import DialogCard from '../../../shared/components/DialogCard/card';

const styles = {
	cardProductTotal: {
		display: 'flex'
	},
	detailsProductTotal: {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
		textAlign: 'right'
	},
	contentProductTotal: {
		flex: '1 0 auto'
	},
	button: {
		width: '100%'
	}
};

function ProductTotal(props) {
	const { classes } = props;
	const [ open, setOpen ] = React.useState(false);

	return (
		<Card className={classes.cardProductTotal}>
			<div className={classes.detailsProductTotal}>
				<CardContent className={classes.contentProductTotal}>
					<Typography component="h5" variant="h5">
						Live From Space
					</Typography>
					<Typography variant="subtitle1" color="textSecondary">
						Mac Miller
					</Typography>
					<Button
						variant="contained"
						color="primary"
						className={classes.button}
						onClick={() => setOpen(true)}
					>
						Buy Now
					</Button>
				</CardContent>
			</div>
			<DialogCard open={open} close={() => setOpen(false)} />
		</Card>
	);
}

ProductTotal.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired
};

export default withStyles(styles)(ProductTotal);
