import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Grid } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import { unstable_Box as Box } from '@material-ui/core/Box';

import Button from '@material-ui/core/Button';

const styles = (theme) => ({
	root: {
		flexGrow: 1
		// height: '100vh'
	},
	paper: {
		padding: theme.spacing.unit * 2,
		textAlign: 'left',
		color: theme.palette.text.secondary
	},
	resImg: {
		width: '100%'
	}
});

class ProductCartList extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.props.loadCartProducts(this.props.cartProducts);
	}

	render() {
		console.log('CART', this.props.cartProducts);
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<Grid container spacing={24}>
					<Grid item xs={12}>
						<Paper className={classes.paper}>Your Shopping Cart</Paper>
					</Grid>
				</Grid>
				<Grid container spacing={24}>
					<Grid item xs={8}>
						<Paper className={classes.paper}>
							{this.props.cartProducts.map((item) => {
								{
									console.log(item.imageUrl);
								}
								return (
									<Grid container spacing={8}>
										<Grid item xs={6}>
											<img className={classes.resImg} src={item.imageUrl} alt="productImg" />
										</Grid>
										<Grid item xs={6}>
											details
										</Grid>
									</Grid>
								);
							})}
						</Paper>
					</Grid>
					<Grid item xs={4}>
						<Paper className={classes.paper}>Grand Total</Paper>
					</Grid>
				</Grid>
			</div>
		);
	}
	// props.loadCartProducts(props.cartproducts);
	// console.log('CART', props);
}

ProductCartList.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired
};

export default compose(withStyles(styles, { withTheme: true }))(ProductCartList);
