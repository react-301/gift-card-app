import httpClient from '../../../core/services/baseService/httpClient';

export function fetchCartProducts() {
	return httpClient.get('cart?userId=1');
}
