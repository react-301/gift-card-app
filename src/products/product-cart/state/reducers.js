import { GET_CARTPRODUCTS_SUCCESS } from './actionTypes';

export default function(state = [ {} ], action) {
	switch (action.type) {
		case GET_CARTPRODUCTS_SUCCESS:
			return action.payload;
		default:
			return state;
	}
}
