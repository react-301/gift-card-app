import { takeLatest, put, call } from 'redux-saga/effects';
import { GET_CARTPRODUCTS_REQUEST } from './actionTypes';
import { fetchCartProducts } from '../api/services';
import { getCartProductsSuccess, getCartProductsFailure } from './actions';

// const loadParam = { pageNo: '1', limit: '15' };

export default function* watchGetCartProducts() {
	yield takeLatest(GET_CARTPRODUCTS_REQUEST, getCartProducts);
}

function* getCartProducts(action) {
	try {
		console.log('before fetching products');
		const { data, headers } = yield call(fetchCartProducts, action.payload);
		console.log('after fetching Products', data, headers['x-total-count']);
		yield put(getCartProductsSuccess(data, headers['x-total-count']));
	} catch (error) {
		yield put(getCartProductsFailure(error));
	}
}
