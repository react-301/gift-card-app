const loadCartProducts = (state) => state.cartProducts;

export default loadCartProducts;
