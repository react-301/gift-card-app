import { GET_CARTPRODUCTS_REQUEST, GET_CARTPRODUCTS_SUCCESS, GET_CARTPRODUCTS_FAILURE } from './actionTypes';

export function getCartProducts(params) {
	return { type: GET_CARTPRODUCTS_REQUEST, payload: params };
}

export function getCartProductsSuccess(cartProducts, totalItems) {
	return { type: GET_CARTPRODUCTS_SUCCESS, payload: cartProducts, totalItems };
}

export function getCartProductsFailure(error) {
	return { type: GET_CARTPRODUCTS_FAILURE, payload: { error } };
}
