import httpClient from '../../../core/services/baseService/httpClient';

export function fetchProducts(params) {
	console.log('API PARAMS', params);
	let apiParams = 'products?_page=1&_limit=31';
	if (params.value === true) {
		switch (params.name) {
			case 'BRANDS':
				apiParams += `&brandId=${params.id}`;
				break;
			case 'CATEGORIES':
				apiParams += `&categoryId=${params.id}`;
				break;
			case 'Id':
				apiParams += `&_sort=id&_order=${params.value}`;
				console.log(apiParams);
				break;
			case 'points':
				apiParams += `&_sort=points&_order=${params.value}`;
				break;
			default:
				break;
		}
	} else {
		switch (params.name) {
			case 'Id':
				apiParams += `&_sort=id&_order=${params.value}`;
				console.log(apiParams);
				break;
			case 'points':
				apiParams += `&_sort=points&_order=${params.value}`;
				break;
			case 'search':
				apiParams += `&q=${params.value}`;
				break;
			case 'pagination':
				apiParams = `products?_page=${params.pageNo}&_limit=31`;
				break;
			default:
				break;
		}
		// return httpClient.get(`products?_page=${params.pageNo}&_limit=${params.limit}`);
		// return httpClient.get(`products`);
	}
	return httpClient.get(apiParams);
}

export function fetchProductsBrand() {
	return httpClient.get('brands');
}

export function fetchCategory() {
	return httpClient.get('categories');
}