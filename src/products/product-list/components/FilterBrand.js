import React, { useState } from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
	root: {
		marginTop: '2%',
		overflowY: 'auto'
	},
	list: {
		height: '271px'
	},
	listHeight: {
		padding: '0px',
		height: '31px',
		margin: '0px'
	}
});

function FilterProductsBrand(props) {
	// console.log('Prod brand', props.productsBrand);
	const { classes, onFilter } = props;
	const [ checked, setChecked ] = useState(false);
	const handleChange = (id) => (event) => {
		console.log(id, event.target.checked);
		setChecked(event.target.checked);
		onFilter({ id: id, value: event.target.checked, name: props.title });
	};
	return (
		<React.Fragment>
			<Typography variant="h6" gutterBottom>
				{props.title}
			</Typography>
			<div className={classes.root}>
				<Grid className={classes.list} item xs={12}>
					{Object.keys(props.productsBrand).map((key) => {
						return (
							<ul key={key} className={classes.listHeight}>
								<li key={key}>
									<FormControlLabel
										control={
											<Checkbox
												checked1={checked}
												onChange={handleChange(props.productsBrand[key].id)}
												value={props.productsBrand[key].name}
											/>
										}
										label={props.productsBrand[key].name}
									/>
								</li>
							</ul>
						);
					})}
				</Grid>
			</div>
		</React.Fragment>
	);
}

FilterProductsBrand.propTypes = {
	classes: PropTypes.object.isRequired,
	width: PropTypes.string.isRequired
};

export default compose(withStyles(styles), withWidth())(FilterProductsBrand);
