import React, { useState } from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import withWidth from '@material-ui/core/withWidth';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import GiftControlCard from '../../../shared/components/ProductCard/productCard';
import Pagination from '../../../shared/components/Pagination/pagination';
import SortGrid from '../../../shared/components/SortList/sort';
import Divider from '@material-ui/core/Divider';
import FilterProductsBrand from '../components/FilterBrand';
import Search from '../../../shared/components/SearchPaper/search';

const styles = (theme) => ({
	root: {
		flexGrow: 1,
		padding: '8px'
	},
	cardheight: {
		height: 240
	},
	container: {
		display: 'flex'
	},
	paper: {
		padding: theme.spacing.unit * 2,
		color: theme.palette.text.secondary,
		flex: '1 0 auto',
		// marginLeft: '2px',
		height: 'auto'
	},
	title: {
		color: 'black',
		fontSize: '21px'
	},
	filter: {
		marginBottom: '4%'
	}
});

class ProductListContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.props.loadProducts(this.props.products);
		this.props.loadProductsBrand(this.props.productsBrand);
		this.props.loadCategory(this.props.category);
	}

	handleFilter = (filterParams) => {
		this.props.loadProducts(filterParams);
	};

	render() {
		const { classes, productsBrand, categories, products, totalItems, loggedIn, userDetails } = this.props;

		console.log('D', products, productsBrand, products.totalItems, loggedIn, userDetails);

		return (
			<div className={classes.root}>
				<Grid container spacing={16}>
					<Grid item xs={8}>
						<SortGrid onSort={this.handleFilter} />
					</Grid>
					<Grid item xs={4}>
						<Search onSearch={this.handleFilter} />
					</Grid>
				</Grid>
				<Grid container spacing={16}>
					<Hidden only="xs">
						<Grid item sm={4} md={3} lg={2}>
							<Paper className={classes.paper}>
								<div className={classes.filter}>
									<Grid item xs={12}>
										<b className={classes.title}>FILTERS</b>
									</Grid>
								</div>
								<Divider />
								<FilterProductsBrand
									productsBrand={productsBrand}
									onFilter={this.handleFilter}
									title={'BRANDS'}
								/>
								<br />
								<Divider />
								<br />
								<FilterProductsBrand
									productsBrand={categories}
									onFilter={this.handleFilter}
									title={'CATEGORIES'}
								/>
							</Paper>
						</Grid>
					</Hidden>
					<Grid item xs={12} sm={8} md={9} lg={10}>
						<Grid container spacing={16}>
							{Object.keys(products).map((key, index) => {
								{
									/* console.log(products[key]); */
								}
								return products[key] !== {} ? (
									<Grid key={index} item xs={12} sm={6} md={6} lg={3}>
										<GiftControlCard
											value={products[key]}
											isLogged={loggedIn}
											userDetails={userDetails}
											role={'user'}
										/>
									</Grid>
								) : null;
							})}

							<Grid item xs={12}>
								<Hidden>
									<Grid xs={4} />
								</Hidden>
								<Grid xs={12} sm={12} md={4} lg={4}>
									<Pagination totalItems={products.totalItems} onPageChange={this.handleFilter} />
								</Grid>
								<Hidden>
									<Grid xs={4} />
								</Hidden>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</div>
		);
	}
}

ProductListContainer.propTypes = {
	classes: PropTypes.object.isRequired,
	width: PropTypes.string.isRequired
};

export default compose(withStyles(styles), withWidth())(ProductListContainer);
