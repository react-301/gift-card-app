import { GET_PRODUCTS_SUCCESS, GET_PRODUCTSBRAND_SUCCESS } from './actionTypes';

export default function(state = [ {} ], action) {
	switch (action.type) {
		case GET_PRODUCTS_SUCCESS:
			return { ...action.payload };
		default:
			return state;
	}
}
