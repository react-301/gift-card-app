import { takeLatest, put, call, race, cancelled } from 'redux-saga/effects';
import { GET_PRODUCTS_REQUEST } from './actionTypes';
import { fetchProducts } from '../api/services';
import { getProductsSuccess, getProductsFailure } from './actions';

// const loadParam = { pageNo: '1', limit: '15' };

export default function* watchGetProducts() {
	yield takeLatest(GET_PRODUCTS_REQUEST, getProducts);
}

function* getProducts(action) {
	try {
		console.log('before fetching products');
		const { data, headers } = yield call(fetchProducts, action.payload);
		console.log('after fetching Products', data, headers['x-total-count']);
		yield put(getProductsSuccess(data, headers['x-total-count']));
	} catch (error) {
		yield put(getProductsFailure(error));
	}
}
