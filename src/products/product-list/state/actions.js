import {
	GET_PRODUCTS_REQUEST,
	GET_PRODUCTS_SUCCESS,
	GET_PRODUCTS_FAILURE,
	GET_PRODUCTSBRAND_REQUEST,
	GET_PRODUCTSBRAND_SUCCESS,
	GET_PRODUCTSBRAND_FAILURE
} from './actionTypes';

export function getProducts(params) {
	return { type: GET_PRODUCTS_REQUEST, payload: params };
}

export function getProductsSuccess(products, totalItems) {
	return { type: GET_PRODUCTS_SUCCESS, payload: { ...products, totalItems } };
}

export function getProductsFailure(error) {
	return { type: GET_PRODUCTS_FAILURE, payload: { error } };
}

// export function getProductsBrand() {
// 	return { type: GET_PRODUCTSBRAND_REQUEST };
// }

// export function getProductsBrandSuccess(productsBrand) {
// 	return { type: GET_PRODUCTSBRAND_SUCCESS, payload: { ...productsBrand } };
// }

// export function getProductsBrandFailure(error) {
// 	return { type: GET_PRODUCTSBRAND_FAILURE, payload: { error } };
// }
