import { createSelector } from 'reselect';

const loadProducts = (state) => state.products;

export default loadProducts;
