import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProducts } from '../state/actions';
import ProductListContainer from '../components/ProductList';
// import ReviewList from '../../../shared/components/ReviewList/ReviewList';
import loadProducts from '../state/selectors';
import loadProductsBrand from '../../../brands/brand-list/state/selector';
import { getBrand } from '../../../brands/brand-list/state/actions';
import withBase from 'core/components/withBase';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';
import loadCategories from '../../../categories/state/selectors';
import { getCategories } from '../../../categories/state/actions';
import { isAuthenticated, populateUser } from '../../../core/security/state/selectors';

const API_ENTITIES = [ 'GET_PRODUCTS', 'GET_BRAND', 'GET_CATEGORIES' ];

const mapStateToProps = (state) => {
	const products = loadProducts(state);
	const productsBrand = loadProductsBrand(state);
	const categories = loadCategories(state);
	return {
		products,
		productsBrand,
		categories,
		userDetails: populateUser(state),
		loggedIn: isAuthenticated(state),
		loading: getAPILoadingStatus(API_ENTITIES)(state),
		error: getAPIErrorMessage(API_ENTITIES)(state)
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadProducts: bindActionCreators(getProducts, dispatch),
		loadProductsBrand: bindActionCreators(getBrand, dispatch),
		loadCategory: bindActionCreators(getCategories, dispatch)
	};
};

const connectProductListPage = connect(mapStateToProps, mapDispatchToProps)(withBase(ProductListContainer));

export default connectProductListPage;
