import httpClient from '../../../core/services/baseService/httpClient';

export function fetchProduct(productId) {
	return httpClient.get(`products/${productId}`);
}
