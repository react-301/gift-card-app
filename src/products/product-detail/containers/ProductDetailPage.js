import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProduct } from '../state/actions';
import { getProductReviews } from '../../../shared/state/ReviewList/state/action';
// import ReviewList from '../../../shared/components/ReviewList';
import loadProduct from '../state/selectors';
import loadProductReviews from '../../../shared/state/ReviewList/state/selecter';
import ProductDetailForm from '../components/ProductDetailForm';
import getAPILoadingStatus from 'core/state/selectors/loadingSelectors';
import getAPIErrorMessage from 'core/state/selectors/errorSelector';
import withBase from 'core/components/withBase';
import { isAuthenticated, populateUser } from '../../../core/security/state/selectors';

class ProductDetailPage extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.props.loadProduct(this.props.productId);
		this.props.loadProductReviews(this.props.productId);
	}

	render() {
		const { product, productReviews, loggedIn } = this.props;
		// const reviews = [];
		console.log('data', product, productReviews);
		return (
			<div>
				<ProductDetailForm product={product} productReviews={productReviews} loggedIn={loggedIn} />
				{/* <ReviewList reviews={reviews} /> */}
			</div>
		);
	}
}

const API_ENTITIES = [ 'GET_PRODUCT', 'ADD_TO_CART' ];

const mapStateToProps = (state, ownProps) => {
	const productId = ownProps.match.params.productId;
	const product = loadProduct(state);
	const productReviews = loadProductReviews(state);
	return {
		productId,
		product,
		productReviews,
		userDetails: populateUser(state),
		loggedIn: isAuthenticated(state),
		loading: getAPILoadingStatus(API_ENTITIES)(state),
		error: getAPIErrorMessage(API_ENTITIES)(state)
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadProduct: bindActionCreators(getProduct, dispatch),
		loadProductReviews: bindActionCreators(getProductReviews, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(withBase(ProductDetailPage));
