import { GET_PRODUCT_SUCCESS } from './actionTypes';

const initialState = {
	title: '',
	description: '',
	rating: 0,
	points: 0,
	quantity: 1
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_PRODUCT_SUCCESS:
			// console.log('SUCCESS', ...action.payload);
			return { ...action.payload };
		default:
			return state;
	}
}
