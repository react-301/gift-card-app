import { GET_PRODUCT_REQUEST, GET_PRODUCT_SUCCESS, GET_PRODUCT_FAILURE } from './actionTypes';

export function getProduct(productId) {
	return { type: GET_PRODUCT_REQUEST, payload: { productId } };
}

export function getProductSuccess(product, reviews) {
	return { type: GET_PRODUCT_SUCCESS, payload: { ...product, ...reviews } };
}

export function getProductFailure(error) {
	return { type: GET_PRODUCT_FAILURE, payload: { error } };
}
