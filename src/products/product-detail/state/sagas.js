import { takeLatest, put, call, race, cancelled } from 'redux-saga/effects';
import { GET_PRODUCT_REQUEST } from './actionTypes';
import { fetchProduct } from '../api/services';
import { getProductSuccess, getProductFailure } from './actions';

export default function* watchGetProduct() {
  yield takeLatest(GET_PRODUCT_REQUEST, getProduct);
}

function* getProduct(action) {
  try {
    console.log('before fetch product');
    const { data } = yield call(fetchProduct, action.payload.productId);
    console.log('after fetchProduct', data);
    yield put(getProductSuccess(data));
  } catch (error) {
    yield put(getProductFailure(error));
  }
}
