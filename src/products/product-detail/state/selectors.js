import { createSelector } from 'reselect';

const loadProduct = (state) => state.product;

export default loadProduct;
