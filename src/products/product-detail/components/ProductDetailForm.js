import React, { useState } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import withWidth from '@material-ui/core/withWidth';
// import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import AttachMoney from '@material-ui/icons/AttachMoney';
import StarRate from '@material-ui/icons/StarRate';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import DialogCard from '../../../shared/components/DialogCard/card';
import ReviewList from '../../../shared/components/ReviewList/ReviewList';
import SnackBar from '../../../shared/components/Snackbar/SnackBar';

const styles = (theme) => ({
	root: {
		flexGrow: 1,
		height: '100vh',
		overflowY: 'auto',
		overflowX: 'hidden'
	},
	cardheight: {
		height: 340
	},
	container: {
		display: 'flex',
		marginTop: '1%'
	},
	paper: {
		padding: theme.spacing.unit * 2,
		color: theme.palette.text.secondary,
		flex: '1 0 auto',
		marginLeft: '2px',
		height: '100%',
		display: 'flex'
	},
	title: {
		color: 'black',
		fontSize: '21px'
	},
	rating: {
		border: '1px solid #a9a9a9',
		paddingLeft: '1%',
		paddingRight: '1%',
		width: 'max-content',
		display: 'inline-flex',
		height: '27px'
	},
	points: {
		width: 'max-content',
		display: 'inline-flex'
	},
	resImg: {
		height: '389px'
	},
	button: {
		margin: theme.spacing.unit,
		height: '61px',
		marginLeft: '0px'
	},
	actnBtn: {
		paddingTop: '4%'
	}
});

function ProductDetailForm(props) {
	const { classes, loggedIn } = props;
	const [ open, setOpen ] = React.useState(false);
	const [ openSnack, setOpenSnack ] = React.useState(false);
	const [ onComplete, setOnComplete ] = React.useState(false);
	console.log('PRODUCT DETAIL', props, props.productReviews, loggedIn);

	const handleBuy = () => {
		if (loggedIn) {
			setOpen(true);
		} else {
			props.history.push('/login');
		}
	};

	const handleCart = () => {
		if (loggedIn) {
			// setOpen(true);
		} else {
			props.history.push('/products');
		}
	};

	if (onComplete) {
		props.history.push('/users/buyer/home');
	}

	return (
		<div className={classes.root}>
			<Grid container spacing={8}>
				<Grid item xs={12} sm={5}>
					<Paper className={classes.paper}>
						<img className={classes.resImg} src={props.product.imageUrl} alt="productImg" />
					</Paper>
				</Grid>
				<Grid xs={12} sm={7}>
					<Paper className={classes.paper}>
						<Grid conatiner xs={12}>
							<Grid item xs={12}>
								<b style={{ fontSize: '21px' }}>{props.product.name}</b>
							</Grid>
							<br />
							<Grid item xs={12}>
								<p>{props.product.desc}</p>
							</Grid>
							<br />
							<Grid item className={classes.rating} xs={12}>
								<div>{props.product.rating}</div>
								<div>
									<StarRate />
								</div>
							</Grid>
							<br />
							<br />
							<Grid item className={classes.points}>
								<div>
									<AttachMoney />
								</div>
								<div>{props.product.points}</div>
							</Grid>
							{/* <Grid item>
								<div>
									{props.product.expiryDays}
								</div>
							</Grid> */}
							<Grid item className={classes.actnBtn}>
								<Button
									variant="contained"
									color="primary"
									className={classes.button}
									onClick={handleCart}
								>
									ADD TO CART
								</Button>
								<Button
									variant="contained"
									color="secondary"
									className={classes.button}
									onClick={handleBuy}
								>
									BUY NOW
								</Button>
							</Grid>
							<br />
							<Divider />
							<br />
							<Grid item xs={12}>
								<b>Customer Reviews:</b>
							</Grid>
							<br />
							<Divider />
							<Grid item xs={12}>
								<ReviewList productReviews={props.productReviews} />
							</Grid>
						</Grid>
					</Paper>
				</Grid>
				<SnackBar
					onComplete={onComplete}
					open={openSnack}
					close={() => setOpenSnack(false)}
					completeClose={() => setOnComplete(true)}
					msg="Order Placed Successfully!"
				/>
				<DialogCard
					open={open}
					close={(res) => {
						//console.log('SENT', res);
						setOpen(false);
						res ? setOpenSnack(true) : setOpenSnack(false);
					}}
				/>
			</Grid>
		</div>
	);
}

ProductDetailForm.propTypes = {
	classes: PropTypes.object.isRequired,
	width: PropTypes.string.isRequired
};

export default compose(withStyles(styles), withWidth())(withRouter(ProductDetailForm));
