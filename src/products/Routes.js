import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ProductDetailPage from './product-detail/containers/ProductDetailPage';
import ProductListPage from './product-list/containers/ProductListPage';
import Loadable from 'react-loadable';
import Loading from 'shared/components/Spinner';

const LoadableProductListPage = Loadable({
	loader: () => import('products/product-list/containers/ProductListPage'),
	loading: Loading
});

const LoadableProductDetailPage = Loadable({
	loader: () => import('products/product-detail/containers/ProductDetailPage'),
	loading: Loading
});

const LoadableProductCartPage = Loadable({
	loader: () => import('products/product-cart/containers/ProductCartPage'),
	loading: Loading
});

export default function() {
	return (
		<Switch>
			<Route exact path="/products" component={ProductListPage} />
			<Route exact path="/products/:productId" component={ProductDetailPage} />
			{/* <Route exact path="/products" component={LoadableProductListPage} /> */}
			{/* <Route
        exact
        path="/products/:productId"
        component={LoadableProductDetailPage}
      /> */}
			{/* <Route exact path="/cart" component={LoadableProductCartPage} /> */}
		</Switch>
	);
}
