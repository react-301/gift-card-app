const manageTranslations = require('react-intl-translations-manager').default;

manageTranslations({
  messagesDirectory: 'translations/messages',
  translationsDirectory: 'src/translations/locales/',
  languages: ['fr', 'en'],
});
